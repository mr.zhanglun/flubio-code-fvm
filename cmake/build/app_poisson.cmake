
set( app "flubio_poisson" )

set( src ${flubio_src}/solvers/POISSON.f03 )

include( ${cmake_local}/build/app_0_post.cmake )
