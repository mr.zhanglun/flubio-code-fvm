
set( flubio_include_dirs
  ${inc_dir_mpi}
  ${inc_dir_petsc}
  ${CMAKE_Fortran_MODULE_DIRECTORY}
)

set( flubio_lib_dirs 
  ${install_dir_flubio}/lib 
)

#file( GLOB_RECURSE flubio_lib_names "${flubio_install_dir}/lib/*.a" )

set( flubio_lib_names 
  solversbuild
  flubiosettings
  poissoneqn
  potentialeqn
  burgerseqn
  convectiondiffusioneqn
  ibmmls
  flubioibm
  serialvtkformat
  meshpartitioning
  parallelvtkformat
)

#debug_DUMP( flubio_lib_names )
