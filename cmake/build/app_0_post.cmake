
set_source_files_properties( ${src} PROPERTIES LANGUAGE Fortran )

add_executable( ${app} ${src} )

set_target_properties( ${app} PROPERTIES COMPILE_FLAGS ${CMAKE_Fortran_FLAGS_RELEASE} )

target_include_directories( ${app} PRIVATE 
  ${flubio_include_dirs}
  ${thirdparty_inc_dirs}
)

target_link_directories( ${app} PRIVATE 
  ${thirdparty_lib_dirs}
  ${flubio_lib_dirs}
)

target_link_libraries( ${app} PRIVATE 
  ${thirdparty_lib_names}
  ${flubio_lib_names}
)

install( TARGETS ${app}
  RUNTIME DESTINATION  ${install_dir_flubio}/bin
)
