#! /bin/bash

# Folders 
casedir=${PWD}
postDir=$casedir/postProc
restDir=$postDir/restart
fieldDir=$postDir/fields
monDir=$postDir/monitors
vtkDir=$postDir/VTKfields
pltDir=$postDir/tecfields

#clean all

echo
echo "Cleaning case..."
echo

rm -rf $monDir/*
rm -rf $restDir/*
rm -rf $fieldDir/*
rm -rf $vtkDir/*
rm -rf $pltDir/*



