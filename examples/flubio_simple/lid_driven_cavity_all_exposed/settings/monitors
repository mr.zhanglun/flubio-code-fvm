!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

{

"Monitors":{

    "Forces":{

       "type": "forceCoeffs",
       "active": true,
       "outputFrequency": 5,
       "patches": ["wall1", "wall2", "wall3", "topWall"], 
       "refDensity": 1.0,
       "refVelocity": 1.0,
       "refPressure": 0,
       "refArea": 1
    },

    "Cp":{

       "type": "forceCoeffs",
       "active": true,
       "outputFrequency": 99,
       "patches": ["wall1"],
       "refDensity": 1.0,
       "refVelocity": 1.0,
       "refPressure": 0,
       "refArea": 1
    },

!----------------!
! Derived fields !
!----------------!

    "vorticity":{
       "type": "vorticity",
       "active": true,
       "saveEvery": 10
    },

    "CP0":{
       "type": "cp0",
       "active": true,
       "saveEvery": 10
    },

!------------------!
! Runtime sampling !
!------------------!

    "probes":{
       "type": "probes",
       "active": true,
       "interpolation": "faceToProbe",
       "fields": ["U", "P"],
       "nProbes": 3,
       "x": [0.25, 0.5, 0.75],
       "y": [0.25, 0.65, 0.85],
       "z": [0.5, 0.5, 0.5],
       "sampleEvery": 10
    },

    "lineProbes":{
       "type": "lineProbes",
       "active": true,
       "interpolation": "faceToProbe",
       "fields": ["U", "P"],
       "startPoint": [0.5, 0.0, 0.5],
       "endPoint": [0.5, 1.0, 0.5], 
       "nProbes": 100,
       "sampleEvery": 10
    }

!    "stlSurface":{
!       "type": "sampledSurfaces",
!       "active": true,
!       "interpolation": "faceToProbe",
!       "fields": ["U", "P"],
!       "surfaces": ["mySurface.stl"],
!       "saveEvery": 10
!    }


}

}

