!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

    subroutine pressureCorrectionEquation

    !==================================================================================================================
    ! Description:
    !! pressureEq assembles and solves the pressure-correction equation.
    !==================================================================================================================

        use flubioDictionaries
        use fieldvar
        use pressureCorrection
        use operators, only : div

        implicit none

        integer :: iCorr, iElement
    !------------------------------------------------------------------------------------------------------------------

        if(steadySim==1) call checkContinuity()

        call Peqn%createEqCoeffs()
        call Peqn%resetCoeffs()

        !----------------------------------------!
        ! Assemble pressure correction equation  !
        !----------------------------------------!

        call Peqn%assemblePressureCorrectionEq()

        call Peqn%pressureBoundaryConditions()

        Peqn%bC(:,1) = -div(mf)
        Peqn%massResidual = Peqn%bC

        !------------------------------------!
        ! Solve pressure correction equation !
        !------------------------------------!

        do iCorr=1,flubioOptions%nCorr+1

            call Peqn%setPressureReference()

            call Peqn%solve(pcorr, setGuess=1, reusePC=flubioSolvers%flag_pcp, reuseMat=iCorr)

            ! Reset rhs to mass residual, get rid of the old corrections
            Peqn%bC = Peqn%massResidual

            ! Add cross terms
            call Peqn%updatePressureCrossTerm(pcorr)

            ! Reuse the same preconditioner in the non-orthogonal corrections
            flubioSolvers%flag_pcp=1

        enddo

        ! Clean up memory
        call Peqn%clearMatAndRhs()
        call Peqn%destroyEqCoeffs()

        ! Reset the preconditioner flag
        if(steadySim==1 .and. mod(itime,flubioSolvers%resetPC)==0) flubioSolvers%flag_pcp=0

    end subroutine pressureCorrectionEquation

!**********************************************************************************************************************

    subroutine pressureEquation

    !==================================================================================================================
    ! Description:
    !! pressureEq assembles and solves the pressure equation used in the modified SIMPLE.
    !==================================================================================================================

        use flubioDictionaries
        use fieldvar
        use pressureCorrection
        use operators, only : div

        implicit none

        integer :: iCorr, iElement, ierr
    !------------------------------------------------------------------------------------------------------------------

        if(steadySim==1) call checkContinuity()

        call Peqn%createEqCoeffs()
        call Peqn%resetCoeffs()

        !----------------------------------------!
        ! Assemble pressure correction equation  !
        !----------------------------------------!

        call Peqn%assemblePressureEq()

        call Peqn%pressureBoundaryConditions()

        Peqn%bC(:,1) = -div(mf)
        Peqn%massResidual = Peqn%bC

        !------------------------------------!
        ! Solve pressure correction equation !
        !------------------------------------!

        do iCorr=1,flubioOptions%nCorr+1

            ! Set pressure reference
            call Peqn%setPressureReference()

            ! Solve equation
            call Peqn%solve(pressure, setGuess=1, reusePC=flubioSolvers%flag_pcp, reuseMat=iCorr)

            ! Reset rhs to mass residual, get rid of the old corrections
            Peqn%bC = Peqn%massResidual
            
            ! Add cross terms
            call Peqn%updatePressureCrossTerm(pressure)

            ! Reuse the same preconditioner in non-orthogonal corrections
            flubioSolvers%flag_pcp=1

        enddo

        call Peqn%clearMatAndRhs()
        call Peqn%destroyEqCoeffs()

        ! Reset the preconditioner flag
        if(steadySim==1 .and. mod(itime,flubioSolvers%resetPC)==0) flubioSolvers%flag_pcp=0

    end subroutine pressureEquation

!**********************************************************************************************************************

    subroutine checkContinuity

    !==================================================================================================================
    ! Description:
    !! checkContinuity calculates the residual for the continuity equation as proposed in Fluent's documentation.
    !==================================================================================================================

        use pressureCorrection
        use globalTimeVar

        integer :: iElement, iFace, iBFace, iBoundary, iOwner, iNeighbour, pNeigh, iProc, is, ie, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: local_ref, local_res, ref, res, resmax, scaledRes, small

        real :: sbuf, rbuf, massRes(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        massRes = 0.0

        ! Internal Faces
        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            massRes(iOwner) = massRes(iOwner) + mf(iFace,1)
            massRes(iNeighbour) = massRes(iNeighbour) - mf(iFace,1)

        enddo

        ! Processor Bondaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie
                pNeigh = pNeigh+1
                iOwner = mesh%owner(iBFace)
                massRes(iOwner) = massRes(iOwner) + mf(iBFace,1)
            enddo

        enddo

        ! Real Boundaries
        do iBoundary=1,numberOfRealBound
            is = mesh%boundaries%startFace(iBoundary)
            ie = is + mesh%boundaries%nFace(iBoundary)-1
            ! Loop over the faces of boundary number "iBoundary"
            do iBFace=is, ie
                iOwner = mesh%owner(iBFace)
                massRes(iOwner) = massRes(iOwner) + mf(iBFace,1)
            enddo
        enddo

        ! Sum up all the mass imbalances
        local_res = 0.0

        do iElement=1,numberOfElements
            local_res = local_res + abs(massRes(iElement))
        enddo

        ! Sum up contribution from different processor's domains
        sbuf = 0.0
        rbuf = 0.0

        ! Fill the buffer array
        sbuf = local_res
        call mpi_allReduce(sbuf, rbuf ,1, MPI_REAL8, MPI_SUM ,MPI_COMM_WORLD, ierr)
        res = rbuf

        if(itime <=5 ) then
            if(res>Peqn%res5) Peqn%res5=res
            Peqn%resCont = 1.0
        elseif(itime > 5) then
            scaledRes = res/Peqn%res5
            Peqn%resCont = scaledRes
        endif

    end subroutine checkContinuity
