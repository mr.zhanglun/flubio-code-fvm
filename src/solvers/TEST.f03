!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

    program testFCT

    !==================================================================================================================
    ! Description:
    !! Driver for test equation solver.
    !==================================================================================================================

#include "petsc/finclude/petscksp.h"
        use petscksp

        use flubioMpi
        use flubioSettings
        use meshvar
        use fluxCorrectedTransport
        use levelSet

        implicit none

        integer :: ierr

        type(fct) :: myFCT

        type(ls) :: myLS
    !------------------------------------------------------------------------------------------------------------------

        !-------------------------!
        ! Initialize parallel run !
        !-------------------------!

        call PetscInitialize(PETSC_NULL_CHARACTER,ierr)

        ! Initialize MPI variables
        call setMPIvar()

        !---------------!
        ! Read settings !
        !---------------!

        call poisson_settings()

        !-----------!
        ! Read mesh !
        !-----------!

        call mesh%readMeshFromFiles()

        call mesh%createMeshSearchTree()

        !--------------------!
        ! Allocate Variables !
        !--------------------!

        call initialiseAdvectionDiffusionSolver()

        call setT()

        call myFCT%createFCT(nComp=1)

        call myLS%createLS()

        !-----------------!
        ! Start time loop !
        !-----------------!

        totalTime=0.0

        do while (time<tmax)

            ! Time-step clock
            telap(1) = mpi_Wtime()

            ! Advance one time-step
            if(steadySim == 0) itime = itime+1
            time = time+dtime
            dtime0 = dtime
            dtime00 = dtime0

            call testEqn(myFCT, myLS)

            ! Time-step clock
            telap(2) = mpi_Wtime()

            ! Save field to file
            if((mod(itime,flubioControls%iflds)==0 .or. abs(flubioControls%iflds)==1) .and. steadySim==0) then
                call testSaveToFile()
                call myLS%psiLS%writeToFile()
            end if

            if(steadySim==0) then
                ! Time-step clock
                telap(2)=mpi_Wtime()
                totalTime = totalTime + telap(2)-telap(1)

                ! print info
                call Teqn%verbose(T)
                call Teqn%printDelimiter()
            endif

            ! Steady state stopping criteria, it allows to keep steady and unsteady within the same file.
            ! Evetual iteration are taken into account the equation itself.
            if(steadySim==1) then
                if(flubioControls%convergence==1) then
                    call flubioMsg('FLUBIO: stopping simulation... ')
                    exit
                end if
            end if

        enddo

        !---------------------!
        ! Fields and restarts !
        !---------------------!

        call testSaveToFile()

        !----------------------!
        ! End of Simulation    !
        !----------------------!

        if(id==0) then
            write(*,*) 'FLUBIO: Simulation compleated',' ', 'total run time =', totalTime
            write(*,*)
        endif

        ! Finilize MPI processing
        call PetscFinalize(ierr)

    end program testFCT

! *********************************************************************************************************************

    subroutine testSaveToFile

        use fieldvar

        implicit none
        
        call T%writeToFile()
        call saveTimeQuantities()

        ! Delete folder you do not want to keep
        call deleteTimeFolders()

    end subroutine

! *********************************************************************************************************************

    subroutine testEqn(myFCT, myLS)

    !==================================================================================================================
    ! Description:
    !! testEqn implements the advection-diffusion equation.
    !==================================================================================================================

        use transportEquation
        use fieldvar
        use globalMeshVar
        use gradient
        use physicalConstants
        use fluxCorrectedTransport
        use levelSet

        implicit none

        type(fct) :: myFCT

        type(ls) :: myLS

        integer :: nCorr, reusePC
    !------------------------------------------------------------------------------------------------------------------

        ! LS test part
        call myLS%updatePsiLS(T)

        ! reset coeffs
        reusePC = 0

        !--------------------------!
        ! Update "Old" terms       !
        !--------------------------!

        call T%updateOldFieldValues()
        call rho%updateOldFieldValues()

        mf = faceFlux(field=velocity, opt=1, coeff=1.0)

        call myfct%FCTbounding(mf, field=T, interpType='gradientbased', interpOpt=2)

        ! Update boundary fields
        call T%updateBoundaryField()

        ! Compute gradient
        call computeGradient(T)

    end subroutine testEqn

!**********************************************************************************************************************

    subroutine setT

        use fieldvar, only: T
        use meshvar

        implicit none

        integer :: iElement

        real :: r

        T%phi = 0.0
        do iElement=1,numberOfElements

            r = sqrt( (mesh%centroid(iElement,1)-0.5)**2 + (mesh%centroid(iElement,2)-0.75)**2 )
            if(r<=0.15) then
                T%phi(iElement,1) = 1.0
            end if

        end do

        call T%writeToFile()

    end subroutine setT