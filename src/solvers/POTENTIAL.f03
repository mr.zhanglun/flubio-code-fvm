!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

program potentialFlow

!==================================================================================================================
! Description:
!! Driver for potential flow solver.
!==================================================================================================================

#include "petsc/finclude/petscksp.h"
	use petscksp

    use flubioMpi
    use flubioSettings
    use meshvar
    use transportEquation

    implicit none

    integer :: ierr
!------------------------------------------------------------------------------------------------------------------

    !-------------------------!
    ! Initialize parallel run !
    !-------------------------!

    call PetscInitialize(PETSC_NULL_CHARACTER,ierr)

    ! Initialize MPI variables
    call setMPIvar()

    !---------------!
    ! Read settings !
    !---------------!

    call poisson_settings()

    !-----------!
    ! Read mesh !
    !-----------!

    call mesh%readMeshFromFiles()

    call mesh%createMeshSearchTree()

    !--------------------!
    ! Allocate Variables !
    !--------------------!
    
    call initialisePotentialSolver()

    !------------------------!
    ! Solve piosson equation !
    !------------------------!

    ! Time-step clock
    telap(1) = mpi_Wtime()
    itime = 0

    call potentialEqn()

    ! Time-step clock
    telap(2) = mpi_Wtime()

    ! Add boundary layers

    ! Save field to file
    call T%writeToFile()
    call velocity%writeToFile()
    call pressure%writeToFile()
    call writeMassFluxesToFile()
    call saveTimeQuantities()

    ! print info
    call Teqn%verbose(T)
    call velocity%fieldVerbose()
    call Teqn%printDelimiter()

    totalTime = telap(2) - telap(1)

    !----------------------!
    ! End of Simulation    !
    !----------------------!

    if(id==0) then
        write(*,*) 'FLUBIO: Simulation completed',' ', 'total run time =', totalTime
        write(*,*)
    endif

    ! Finilize MPI processing
    call PetscFinalize(ierr)

end program potentialFlow

! *********************************************************************************************************************

