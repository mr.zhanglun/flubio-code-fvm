!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module flubioSettings

!==================================================================================================================
! Description:
!! flubioSettings reads settings from a configuration files for the potential/Poisson solvers.
!==================================================================================================================

    use flubioDictionaries
    use flubioMpi

    implicit none

contains

    subroutine poisson_settings

    !==================================================================================================================
    ! Description:
    !! poisson_settings fills the dictionaries needed to setup the Poisson/convection diffusion solver.
    !==================================================================================================================

        call printLogo

        !--------------!
        ! Dictionaries !
        !--------------!

        call flubioControls%set()
        call flubioOptions%setOptions()
        if(steadySim==1) call flubioSteadyStateControls%setForPoisson()
        call flubioSolvers%set()
        call flubioMeshRegions%set()
        call flubioSources%set()

    end subroutine poisson_settings

! *********************************************************************************************************************

    subroutine NavierStokesSettings

    !==================================================================================================================
    ! Description:
    !! NavierStokesSettings fills the dictionaries needed to setup the Navier-Stokes solver
    !==================================================================================================================

        call printLogo

        !--------------!
        ! Dictionaries !
        !--------------!

        call flubioControls%set()
        call flubioOptions%setOptions()
        call flubioSolvers%set()
        if(steadySim==1) call flubioSteadyStateControls%set()
        call flubioTurbModel%set()
        call flubioMonitors%set()
        call flubioMeshRegions%set()
        call flubioSources%set()

    end subroutine NavierStokesSettings

! *********************************************************************************************************************

    subroutine printLogo

        if(id==0) then

            write(*,'(A)')
            write(*,'(A)')   '!*******************************************************************************************!'
            write(*,'(A)')   '!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !'
            write(*,'(A)')   '! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !'
            write(*,'(A)')   '! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !'
            write(*,'(A)')   '! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !'
            write(*,'(A)')   '! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !'
            write(*,'(A)')   '! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !'
            write(*,'(A)')   '!*******************************************************************************************!'

            write(*,'(A)')

            write(*,'(A)') '!*******************************************************************************************!'
            write(*,'(A)') '!                                                                                           !'
            write(*,'(A)') '! License:                                                                                  !'
            write(*,'(A)') '! Creative commons CC BY-NC 2.0                                                             !'
            write(*,'(A)')  '!                                                                                           !'
            write(*,'(A)') '! How to cite:                                                                              !'
            write(*,'(A)') '! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !'
            write(*,'(A)') '! based Navier-Stokes and convection-diffusion like equations solver for teaching           !'
            write(*,'(A)') '! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !'
            write(*,'(A)') '! https://doi.org/10.1016/j.softx.2020.100655                                               !'
            write(*,'(A)') '!                                                                                           !'
            write(*,'(A)') '! Code repository:                                                                          !'
            write(*,'(A)') '! https://gitlab.com/alie89/flubio-code-fvm                                                 !'
            write(*,'(A)') '!                                                                                           !'
            write(*,'(A)') '!*******************************************************************************************!'
            write(*,'(A)')
        endif

    end subroutine printLogo

end module flubioSettings

! *********************************************************************************************************************
