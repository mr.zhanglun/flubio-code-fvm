!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module lsolvers

!==================================================================================================================
! Description:
!! This module implements the dictionary storing the entries of settings/lsolver and associated flags.
!==================================================================================================================

    use generalDictionaries
    use globalMeshVar

        implicit none

    type, extends(generalDict) :: lsolversDict

        integer :: M
        !! Global linear system matrices size (total number of cells x total number of cells)

        integer :: lm
        !! Local linear system matrices size (NumberOfElements x numberOfElements)

        integer :: d_nz
        !! Expected number of non-zero entries in matrix diagonal block (see PETSc manual)

        integer :: o_nz
        !!  Expected number of non-zero entries in matrix oof-diagonal block (see PETSc manual)

        integer :: itmin
        !! Minimum number of iterations to perform

        integer :: itmax
        !! Maxium number of linear solver iterations

        integer :: flag_pcp
        !! Flag for the pressure preconditioner

        integer :: resetPc
        !! Flag to reset the pressure preconditioner

        character(len=:), allocatable :: matOrderingType
        !! Name of the choosen linear solver for the pressure-correction equation

        character(len=:), allocatable :: scalingType
        !! Name of the choosen linear solver for the pressure-correction equation

    contains
        procedure :: set => setGlobalSolverParameters
        procedure :: getSparsity
        procedure :: setPreconditionerOption
        procedure :: setMatrixSize

    end type lsolversDict
    
contains

! *********************************************************************************************************************

    subroutine setGlobalSolverParameters(this)

    !==================================================================================================================
    ! Description:
    !! setGlobalSolverParameters sets some basic solver mandatory options.
    !==================================================================================================================

        class(lsolversDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! dictionary name

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: opt
        !! option value

    !------------------------------------------------------------------------------------------------------------------

        integer :: error_cnt

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'settings/linearSolvers'

        ! Initialise the JSON dictionary
        call this%initJSON(dictName)

        call this%json%get('RenumberMesh', opt, found)
        if(.not. found) opt='none'
        call bandwidthReductionSettings(this, opt)

        ! Default values fot maxIter and minIter
        call this%json%get('MaxIter', this%itmax, found)
        if(.not. found) this%itmax = 1000

        call this%json%get('MinIter', this%itmin, found)
        if(.not. found) then 
             this%itmin = 0
        endif 
        
        call this%json%get('RecomputePCEvery', this%resetPc, found)
        call raiseKeyErrorJSON('RecomputePCEvery', found, dictName)

        call this%json%get('DisplayScaledResidual', this%scalingType, found)
        if(.not. found) this%scalingType = 'no'

        call this%getSparsity()

        call this%setMatrixSize()

    end subroutine setGlobalSolverParameters

! *********************************************************************************************************************

    subroutine bandWidthReductionSettings(this, bandOpt)

    !==================================================================================================================
    ! Description:
    !! bandwidthReductionSettings sets the badwidth reduction method.
    !==================================================================================================================

        type(lsolversDict) :: this
    !------------------------------------------------------------------------------------------------------------------
        
        character(len=:), allocatable :: bandOpt
    !------------------------------------------------------------------------------------------------------------------

        bandOpt = lowercase(bandOpt)

        if(bandOpt=='natural') then
            this%matOrderingType='natural'
        elseif(bandOpt=='nesteddissection') then
            this%matOrderingType='nd'
        elseif(bandOpt=='onewaydissection') then
            this%matOrderingType='1wd'
        elseif(bandOpt=='reversedcuthillmckee') then
            this%matOrderingType='rcm'
        elseif(bandOpt=='quotientminimumdegree') then
            this%matOrderingType='qmd'
        elseif(bandOpt=='rowlenght') then
            this%matOrderingType='rowlenght'
        elseif(bandOpt=='wbm') then
            this%matOrderingType='wbm'
        elseif(bandOpt=='spectral') then
            this%matOrderingType='spectral'
        elseif(bandOpt=='amd') then
            this%matOrderingType='amd'
        else
            if(id==0) then
                write(*,*) 'Warning: readSettings.F95::bandwidthReductionSettings - Unknown badwidth reduction method: ', trim(bandOpt)
                write(*,*) 'Using default one: reversedCuthillMckee'
                this%matOrderingType='rcm'
            endif
        endif
        
    end subroutine bandWidthReductionSettings

! *********************************************************************************************************************

    subroutine setPreconditionerOption(this, pcOpt, pcFlag, solverType)

    !==================================================================================================================
    ! Description:
    !! setPreconditionerOption sets the preconditioner option.
    !==================================================================================================================

        class(lsolversDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: pcOpt

        character(len=*) :: solverType
    !------------------------------------------------------------------------------------------------------------------

        integer :: pcFlag
    !------------------------------------------------------------------------------------------------------------------

        if(lowercase(solverType)=='petsc') then 

            if(pcOpt=='bjacobi') then
                pcFlag = 0
            elseif(pcOpt=='asm') then
                pcFlag = 1
            elseif(pcOpt=='gamg') then
                pcFlag = 2
            elseif(pcOpt=='hypre') then
                pcFlag = 3
            elseif(pcOpt=='pjacobi') then
                pcFlag = 4
            elseif(pcOpt=='sor') then
                pcFlag = 5
            elseif(pcOpt=='spai') then
                pcFlag = 6
            elseif(pcOpt=='kaczmarz') then
                pcFlag = 7
            elseif(pcOpt=='ml') then
                pcFlag = 8
            elseif(pcOpt=='eisenstat') then
                pcFlag = 9
            elseif(pcOpt=='icc') then
                pcFlag = 10
            elseif(pcOpt=='lu') then
                pcFlag = 11
            elseif(pcOpt=='cholesky') then
                pcFlag = 12
            elseif(pcOpt=='fieldsplit') then 
                pcFlag = 13
            elseif(pcOpt=='none') then
                pcFlag = -1
            else
                call flubioStopMsg('FLUBIO ERROR: Unknown petsc precondtioner '//trim(pcOpt))
            endif

        elseif(lowercase(solverType)=='amgcl') then

            if(pcOpt=='amg') then
                pcFlag = 0
            elseif(pcOpt=='schur') then
                pcFlag = 1
            elseif(pcOpt=='deflatedschur') then
                pcFlag = 2
            elseif(pcOpt=='cpr') then
                pcFlag = 3
            else
                call flubioStopMsg('FLUBIO ERROR: Unknown amgcl precondtioner '//trim(pcOpt))
            endif

        else 
            call flubioStopMsg('FLUBIO: unknown solver type '//solverType//'. Available options are petsc or amgcl.') 
        endif

    end subroutine setPreconditionerOption

! *********************************************************************************************************************

    subroutine getSparsity(this)

    !==================================================================================================================
    ! Description:
    !! getSparsity sets the number of diagona and off-diagonal non zero entries in a equation matrix.
    !! The number of non zeros set by this subroutine are useless as now we count the exact number of non-zeros.
    !==================================================================================================================

        class(lsolversDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: d_nz, o_nz
    !------------------------------------------------------------------------------------------------------------------

        this%lm = numberOfElements

        this%d_nz = 0
        this%o_nz = 0

        ! Approximate number of non-zero entries
        this%d_nz = 20
        this%o_nz = 20

    end subroutine getSparsity

!**********************************************************************************************************************

    subroutine setMatrixSize(this)
        
    !==================================================================================================================
    ! Description:
    !! setMatrixSize compute the local and the global matrix size.
    !==================================================================================================================
                    
        class(lsolversDict) :: this
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: sbuf, rbuf, ierr
    !------------------------------------------------------------------------------------------------------------------
            
            this%lm = numberOfElements
            sbuf = this%lm
            
            ! Prepare the sizes
            call mpi_allReduce(sbuf, rbuf, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD, ierr)
            this%M = rbuf
            
    end subroutine setMatrixSize
            
!**********************************************************************************************************************

end module lsolvers