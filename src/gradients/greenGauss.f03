!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module greenGauss

!==================================================================================================================
! Description:
!! greenGauss implements the Green-Gauss method for the computation of the gradient.
!==================================================================================================================

	use flubioDictionaries
	use flubioFields
	use meshvar
	use interpolation

	implicit none

contains

	subroutine greenGaussGrad(field)

	!==================================================================================================================
	! Description:
	!! greenGaussGrad computes the gradient of a field using the Green-Gauss method
	!! and optionally corrects it with  skwness corrections.
	!==================================================================================================================

		type(flubioField) :: field
		!! field used to compute the gradient
	!------------------------------------------------------------------------------------------------------------------

		integer :: n
		!! number of skweness corection

		integer :: fComp
		!! number of field component
	!------------------------------------------------------------------------------------------------------------------

		fComp = field%nComp

		!------------------------------------!
		! Interpolate phi values at CV faces !
		!------------------------------------!

		call interpolateElementToFaceStd(field, flubioOptions%interpOpt)

		!---------------------!
		! Green-Gauss formula !
		!---------------------!

		call gradient0(field%phiGrad, field%phif, fComp)

		!----------------------------------------------!
		! Account for mesh skewness, correct gradient  !
		!----------------------------------------------!

		do n=1,flubioOptions%git

			call field%updateGhostsGradient()

			call correctFaceValue(field%phif, field%phif, field%phiGrad, field%ghostsGrad, fComp)

			! Recompute gradient
			call gradient0(field%phiGrad, field%phif, fComp)

		enddo

	end subroutine greenGaussGrad

! *********************************************************************************************************************

	subroutine greenGaussGradTVD(field, faceLimiter)

	!==================================================================================================================
	! Description:
	!! greenGaussGradTVD computes the gradient of a field using face values from a known TVD scheme.
	!==================================================================================================================

		type(flubioField) :: field
		!! field used to compute the gradient
	!------------------------------------------------------------------------------------------------------------------

		integer :: n
		!! number of skweness corection

		integer :: fComp
		!! number of field component

		integer :: faceLimiter
		!! face limiter to use if tvd green gauss has been selected
	!------------------------------------------------------------------------------------------------------------------

		fComp = field%nComp

		!------------------------------------!
		! Interpolate phi values at CV faces !
		!------------------------------------!

		call interpolate(field, 'tvd', faceLimiter)

		!---------------------!
		! Green-Gauss formula !
		!---------------------!

		call gradient0(field%phiGrad, field%phif, fComp)

		!----------------------------------------------!
		! Account for mesh skewness, correct gradient  !
		!----------------------------------------------!

		do n=1,flubioOptions%git

			call field%updateGhostsGradient()

			call correctFaceValue(field%phif, field%phif, field%phiGrad, field%ghostsGrad, fComp)

			! Recompute gradient
			call gradient0(field%phiGrad, field%phif, fComp)

		enddo

	end subroutine greenGaussGradTVD

! *********************************************************************************************************************

	subroutine gradient0(phiGrad0, phi_f, fComp)

	!==================================================================================================================
	! Description:
	!! gradient0 computes the gradient of a field using the Green-Gauss formula.
	!==================================================================================================================

		integer i, is, ie, pNeigh, patchFace, iBoundary
	!------------------------------------------------------------------------------------------------------------------

		integer iElement, iFace, iBFace, iComp, fComp, iOwner, iNeighbour
	!------------------------------------------------------------------------------------------------------------------

		real :: phiGrad0(numberOfElements+numberOfBFaces, 3, fComp)
		!! field gradient

		real :: phi_f(numberOfFaces, fComp)
		!! field values at mesh faces
	!------------------------------------------------------------------------------------------------------------------

		phiGrad0 = 0.0

		!----------------!
		! Internal faces !
		!----------------!

		do iFace=1,numberOfIntFaces

		   iOwner = mesh%owner(iFace)
		   iNeighbour = mesh%neighbour(iFace)

		   do iComp=1,fComp

			  phiGrad0(iOwner,:,iComp) = phiGrad0(iOwner,:,iComp)+phi_f(iFace,iComp)*mesh%Sf(iFace,:)
			  phiGrad0(iNeighbour,:,iComp) = phiGrad0(iNeighbour,:,iComp)-phi_f(iFace,iComp)*mesh%Sf(iFace,:)

		   enddo

		enddo

		!----------------!
		! Boundary faces !
		!----------------!

		do iBFace=numberOfIntFaces+1,numberOfFaces

		   iOwner = mesh%owner(iBFace)

		   do iComp=1,fComp

			  phiGrad0(iOwner,:,iComp) = phiGrad0(iOwner,:,iComp)+phi_f(iBFace,iComp)*mesh%Sf(iBFace,:)

		   enddo

		enddo

		!---------------------!
		! Divide by CV volume !
		!---------------------!

		do iElement=1,numberOfElements
		   phiGrad0(iElement,:,:) = phiGrad0(iElement,:,:)/mesh%volume(iElement)
		enddo

		!-----------------------!
		! Set boundary gradient !
		!-----------------------!

		patchFace = numberOfElements

		do iBFace=numberOfIntFaces+1,numberOfFaces

		   patchFace = patchFace+1
		   iOwner = mesh%owner(iBFace)

		   ! equal to the owner element, then overwritten by the actual boundary gradients
		   phiGrad0(patchFace,:,:) = phiGrad0(iOwner,:,:)

		enddo

	end subroutine gradient0

! *********************************************************************************************************************

	subroutine correctFaceValue(phi_f, phi_fp, phiGrad0, phiGradGhost0, fComp)

	!==================================================================================================================
	! Description:
	!! correctFaceValue corrects the faces values using the actual gradient in order to reduce skewness errors.
	!! For highly skewed meshes, use least squares method.
	!==================================================================================================================

		integer iFace, iBFace, iBoundary, iComp, fComp, iProc, iOwner, iNeighbour, pNeigh, is, ie
	!------------------------------------------------------------------------------------------------------------------

		real :: phi_f(numberOfFaces,fComp)
		!! field values at mesh faces

		real :: phi_fp(numberOfFaces,fComp)
		!! field values at mesh faces but skew corrected

		real :: corr
		!! skewness correction
	!------------------------------------------------------------------------------------------------------------------

        real :: phiGrad0(numberOfElements+numberOfBFaces, 3, fComp)
		!! uncorrectd field gradient

		real :: grad(3)
		!! field gradient

		real :: rffp(3)
		!! corrected cell to face distance

		real :: phiGradGhost0(maxProcFaces, 3, numberOfProcBound1, fComp)
		!! uncorrectd field gradient at ghost cells
	!------------------------------------------------------------------------------------------------------------------

		phi_f = phi_fp

		do iComp=1, fComp

		  !----------------!
		  ! Internal Faces !
		  !----------------!

		   do iFace=1, numberOfIntFaces

			  iOwner = mesh%owner(iFace)

			  iNeighbour = mesh%neighbour(iFace)

			  if(flubioOptions%skwOpt==0) then

			     ! Compute gradPhi'
				 grad = mesh%skw_gf(iFace)*phiGrad0(iOwner,:,iComp)+ &
									(1-mesh%skw_gf(iFace))*phiGrad0(iNeighbour,:,iComp)

			     ! Update Face value
				 rffp = mesh%fcentroid(iFace,:) - mesh%fcentroid_skw(iFace,:)

			  else

			     ! Compute gradPhi'
				 grad = phiGrad0(iOwner,:,iComp) + phiGrad0(iNeighbour,:,iComp)

			     ! Update Face value
				 rffp = mesh%fcentroid(iFace,:)-0.5*(mesh%centroid(iOwner,:) + mesh%centroid(iNeighbour,:))

			  endif

			  corr = dot_product(grad, rffp)

			  phi_f(iFace,iComp) = phi_fp(iFace,iComp) + corr

		   enddo

			!----------------------!
			! Processor boundaries !
			!----------------------!

			do iBoundary=1,numberOfProcBound

				iProc = mesh%boundaries%procBound(iBoundary)

				is = mesh%boundaries%startFace(iProc)
				ie = is+mesh%boundaries%nFace(iProc)-1

				pNeigh=0

				do iBFace=is,ie

					pNeigh=pNeigh+1

					iOwner = mesh%owner(iBFace)

					if(flubioOptions%skwOpt==0) then

					! Compute gradPhi'
					   grad = mesh%skw_gf(iBFace)*phiGrad0(iOwner,:,iComp) + &
								  (1-mesh%skw_gf(iBFace))*phiGradGhost0(pNeigh,:,iBoundary,iComp)

					! Update Face value
					   rffp = mesh%fcentroid(iBFace,:) - mesh%fcentroid_skw(iBFace,:)

					else

					! Compute gradPhi'
					   grad = phiGrad0(iOwner,:,iComp) + phiGradGhost0(pNeigh,:,iBoundary,iComp)

					! Update Face value
					   rffp = mesh%fcentroid(iBFace,:) - 0.5*(mesh%centroid(iOwner,:) + mesh%ghost(pNeigh,:,iBoundary))

					endif

					   corr = dot_product(grad, rffp)
					   phi_f(iBFace,iComp) = phi_fp(iBFace,iComp) + corr

				enddo

			enddo

		enddo

	end subroutine correctFaceValue

! *********************************************************************************************************************

end module greenGauss