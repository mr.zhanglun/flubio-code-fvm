!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module runTimeObject

    use flubioFields

    implicit none

    type, public :: runTimeObj

        character(len=:), allocatable :: objName
        !! object name

        character(len=:), allocatable :: objType
        !! task type

        integer :: runAt
        !! when to run

        integer :: saveEvery
        !! save statisitcs every N iterations or time-steps

        type(flubioField) :: taskField
        !! task field, if any. I am force to define it here in the base type to add a poiter in the registry.

        contains
            procedure :: initialise =>  initialiseBaseObj
            procedure :: run  => runBaseObj
    end type runTimeObj

    type, public, extends(runTimeObj) :: fieldObj
        character(len=:), allocatable :: fieldName
        integer :: nComp
    contains

        procedure :: initialise => createFieldObj
        procedure :: write => writeFieldObject

    end type fieldObj

contains

    subroutine initialiseBaseObj(this)

    !==================================================================================================================
    ! Description:
    !! runTimeObjects is the base constructor for run-time objects.
    !==================================================================================================================

        class(runTimeObj) :: this
    !------------------------------------------------------------------------------------------------------------------
    
    end subroutine initialiseBaseObj

!**********************************************************************************************************************

    subroutine runBaseObj(this)

    !==================================================================================================================
    ! Description:
    !! runBaseObj is the base runner for run-time objects.
    !==================================================================================================================

        class(runTimeObj) :: this
    !------------------------------------------------------------------------------------------------------------------

    end subroutine runBaseObj

!***********************************************************************************************************************    

    subroutine createFieldObj(this)

    !==================================================================================================================
    ! Description:
    !! createFieldObj is the field constructor and it allocates the memory for a new flubioField.
    !==================================================================================================================
    
        class(fieldObj) :: this
    !-------------------------------------------------------------------------------------------------------------------
    
        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------
    
        type(json_core) :: jcore
    
        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------
    
        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)
    
        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)
    
        ! Get monitor pointer
        call jcore%get_child(monitorsPointer, this%objName, monitorPointer, monitorFound)
    
        call jcore%get(monitorPointer, 'runEvery', this%runAt, found)
        if(.not. found) this%runAt = 1

        call jcore%get(monitorPointer, 'saveEvery', this%saveEvery, found)
        call raiseKeyErrorJSON('saveEvery', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'components', this%nComp, found)
        call raiseKeyErrorJSON('components', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'name', this%fieldName, found)
        call raiseKeyErrorJSON('name', found, 'settings/monitors/'//this%objName)
    
        call this%taskField%createWorkField(fieldName=this%fieldName, classType='scalar', nComp=this%nComp)
    
        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()
    
    end subroutine createFieldObj
    
!***************************************************************************************************************************
    
    subroutine writeFieldObject(this)
    
    !==================================================================================================================
    ! Description:
    !! writeFieldObject writes the field to a file to be post processed.
    !==================================================================================================================
    
        class(fieldObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime,this%saveEvery)==0 .or. abs(this%saveEvery)==1)  call this%taskField%writeToFileW()
    
    end subroutine writeFieldObject

end module runTimeObject

!**********************************************************************************************************************