module Cp0Field

    use flubioDictionaries, only: flubioMonitors, raiseKeyErrorJSON
    use globalMeshVar
    use globalTimeVar
    use fieldvar, only: velocity, pressure
    use flubioMpi
    use physicalConstants, only: rho
    use json_module
    use runTimeObject

    implicit none

    type, public, extends(fieldObj) :: CP0
        real:: rhoRef
        real:: velRef
        logical :: scale
    contains
        procedure :: initialise => createCP0
        procedure :: run => computeCp0
    end type CP0

contains

    subroutine createCP0(this)

    !==================================================================================================================
    ! Description:
    !! createCP0 is the CP0 constructor.
    !==================================================================================================================
    
        class(CP0) :: this
    !-------------------------------------------------------------------------------------------------------------------
    
        character(len=:), allocatable :: scale
    !-------------------------------------------------------------------------------------------------------------------

        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------
    
        type(json_core) :: jcore
    
        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------
    
        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)
    
        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)
    
        ! Get monitor pointer
        call jcore%get_child(monitorsPointer, this%objName, monitorPointer, monitorFound)
    
        this%scale = .false.
        call jcore%get(monitorPointer, 'scale', scale, found)
        
        if(lowercase(scale)=='yes' .or. lowercase(scale)=='on' .or. lowercase(scale)=='true') then 
            this%scale = .true.

            call jcore%get(monitorPointer, 'rhoRef', this%rhoRef, found)
            call raiseKeyErrorJSON('rhoRef', found, 'settings/monitors/'//this%objName)

            call jcore%get(monitorPointer, 'velRef', this%velRef, found)
            call raiseKeyErrorJSON('velRef', found, 'settings/monitors/'//this%objName)

        endif 

        call jcore%get(monitorPointer, 'runEvery', this%runAt, found)
        if(.not. found) this%runAt = 1

        call jcore%get(monitorPointer, 'saveEvery', this%saveEvery, found)
        call raiseKeyErrorJSON('saveEvery', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'components', this%nComp, found)
        call raiseKeyErrorJSON('components', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'name', this%fieldName, found)
        call raiseKeyErrorJSON('name', found, 'settings/monitors/'//this%objName)
    
        call this%taskField%createWorkField(fieldName=this%fieldName, classType='scalar', nComp=this%nComp)
    
        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()
    
    end subroutine createCP0
    
! *********************************************************************************************************************

    subroutine computeCp0(this)

    !==================================================================================================================
    ! Description:
    !! computeCp0 computes the total pressure as:
    !! \begin{equation*}
    !! P_t = p + 0.5*rho|U|^2
    !! \end{equation*}
    !==================================================================================================================

        class(CP0):: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i
    !------------------------------------------------------------------------------------------------------------------

        real :: u, v, w, p, scalingFactor
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%runAt)==0) then
            do i=1,numberOfElements+numberOfBFaces
                u = velocity%phi(i,1)
                v = velocity%phi(i,2)
                w = velocity%phi(i,3)
                p = pressure%phi(i,1)
                if(this%scale) then
                    scalingFactor = 0.5*this%rhoRef*this%velRef**2
                    this%taskField%phi(i,1) = p + 0.5*rho%phi(i,1)*(u**2+v**2+w**2)/scalingFactor
                else
                    this%taskField%phi(i,1) = p + 0.5*rho%phi(i,1)*(u**2+v**2+w**2)
                endif    
            end do
            call this%write()
        endif

    end subroutine computeCp0

!**********************************************************************************************************************
    
end module Cp0Field