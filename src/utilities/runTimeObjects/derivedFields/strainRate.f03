module strainRateField

    use globalMeshVar
    use globalTimeVar
    use fieldvar, only: velocity
    use flubioMpi
    use runTimeObject
    use velocityTensors

    implicit none

    type, public, extends(fieldObj) :: E

    contains
        procedure :: run =>  computeStrainRate
    end type E

contains

! *********************************************************************************************************************

    subroutine computestrainRate(this)

    !==================================================================================================================
    ! Description:
    !! computeStrainRate computes the strain rate tensor module.
    !==================================================================================================================

        class(E):: this
	!------------------------------------------------------------------------------------------------------------------

		this%taskField%phi = 0.0
        if(mod(itime, this%runAt)==0) then
            this%taskField%phi(1:numberOfElements,1) = strainTensorModule()
            call this%write()
        endif

    end subroutine computestrainRate

!**********************************************************************************************************************


end module strainRateField
