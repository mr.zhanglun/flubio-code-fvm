module continuityErrors

    use fieldvar, only: mf
    use flubioMpi
    use operators, only: div
    use runTimeObject

    implicit none

    type, public, extends(fieldObj) :: contErr

        real :: maxLocalErr
        
        real :: maxGlobalErr
        
    contains
        procedure :: run => computeContErr
    end type contErr

contains

! *********************************************************************************************************************

    subroutine computeContErr(this)

    !==================================================================================================================
    ! Description:
    !! computeContErr computes the continuty errors:
    !! \begin{equation*}
    !! error = div(U)
    !! \end{equation*}
    !==================================================================================================================

        class(contErr):: this
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%runAt)==0) then
            this%taskField%phi(1:numberOfElements,1) = div(mf)
            call this%write()
        endif

    end subroutine computeContErr

!**********************************************************************************************************************
    
end module continuityErrors