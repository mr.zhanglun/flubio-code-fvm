module multiplyFields

    use flubioDictionaries, only: flubioMonitors, raiseKeyErrorJSON
    use fieldvar, only: fieldRegistry,getFieldLocationInRegistry
    use json_module
    use runTimeObject

    implicit none

    type, public, extends(fieldObj) :: multiplyObj
        character(len=:), dimension(:), allocatable :: fieldsToMultiply
        character(len=:), allocatable :: operation
    contains
        procedure :: initialise => createMultiplyObj
        procedure :: run => multiplyDivideFields
    end type multiplyObj

contains
        
subroutine createMultiplyObj(this)

    !==================================================================================================================
    ! Description:
    !! createMultiplyObj is the field constructor.
    !==================================================================================================================
    
        class(multiplyObj) :: this
    !-------------------------------------------------------------------------------------------------------------------
    
        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------
    
        type(json_core) :: jcore
    
        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------
    
        integer, dimension(:), allocatable :: ilen
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)
    
        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)
    
        ! Get monitor pointer
        call jcore%get_child(monitorsPointer, this%objName, monitorPointer, monitorFound)
    
        call jcore%get(monitorPointer, 'runEvery', this%runAt, found)
        if(.not. found) this%runAt = 1

        call jcore%get(monitorPointer, 'saveEvery', this%saveEvery, found)
        call raiseKeyErrorJSON('saveEvery', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'components', this%nComp, found)
        call raiseKeyErrorJSON('components', found, 'settings/monitors/'//this%objName)
        
        call jcore%get(monitorPointer, 'fieldsToMultiply', this%fieldsToMultiply, ilen, found)
        call raiseKeyErrorJSON('fieldsToMultiply', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'operation', this%operation, found)
        call raiseKeyErrorJSON('operation', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'name', this%fieldName, found)
        call raiseKeyErrorJSON('name', found, 'settings/monitors/'//this%objName)
    
        call this%taskField%createWorkField(fieldName=this%fieldName, classType='scalar', nComp=this%nComp)
    
        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()
    
    end subroutine createMultiplyObj
    
! *********************************************************************************************************************

    subroutine multiplyDivideFields(this)

    !==================================================================================================================
    ! Description:
    !! multiplyDivideFields multiply or divide the fields specified the list.
    !==================================================================================================================

        class(multiplyObj):: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, fieldIndex
    !------------------------------------------------------------------------------------------------------------------
      
        if(mod(itime, this%runAt)==0) then

            do i=1,size(this%fieldsToMultiply)

                fieldIndex = getFieldLocationInRegistry(trim(this%fieldsToMultiply(i)))

                ! Check the dimensions are consistent
                if(fieldRegistry(fieldIndex)%field%nComp /= this%nComp) call flubioStopMsg("ERROR: dimensio of fields to add are not consistent.")

                if(lowercase(this%operation)=='divide') then
                    this%taskField%phi = this%taskField%phi/fieldRegistry(fieldIndex)%field%phi
                else
                    this%taskField%phi = this%taskField%phi*fieldRegistry(fieldIndex)%field%phi
                endif    

                call this%write()

            enddo
            
        endif
        
    end subroutine multiplyDivideFields

!**********************************************************************************************************************

end module multiplyFields