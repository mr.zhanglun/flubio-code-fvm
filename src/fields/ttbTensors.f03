module ttbTensors
    
    use tensorAddition
    use tensorMultiply
    use tensorDyadic
    use tensorMath

end module ttbTensors