module tensorAddition

    use flubioTensors

    implicit none

    interface operator (+)
        module procedure :: add11
        module procedure :: add22
        module procedure :: add44
        module procedure :: add2s2s
        module procedure :: add22s
        module procedure :: add2s2
        module procedure :: add4s4s
        module procedure :: add44s
        module procedure :: add4s4
    end interface operator (+)

    interface operator (-)
        module procedure :: sub11
        module procedure :: sub22
        module procedure :: sub44
        module procedure :: sub2s2s
        module procedure :: sub22s
        module procedure :: sub2s2
        module procedure :: sub4s4s
        module procedure :: sub44s
        module procedure :: sub4s4
    end interface operator (-)

contains 

!**********************************************************************************************************************

    function add11(T1, T2) result(T3)

        type(ttbTensor1), intent(IN) :: T1, T2
        
        type(ttbTensor1) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) + T2%T(iElement)
        enddo

    end function add11

!**********************************************************************************************************************

    function add22(T1, T2) result(T3)

        type(ttbTensor2), intent(IN) :: T1, T2
        
        type(ttbTensor2) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) + T2%T(iElement)
        enddo

    end function add22

!**********************************************************************************************************************

    function add44(T1, T2) result(T3)

        type(ttbTensor4), intent(IN) :: T1, T2
        
        type(ttbTensor4) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) + T2%T(iElement)
        enddo

    end function add44

!**********************************************************************************************************************

    function add2s2s(T1, T2) result(T3)

        type(ttbTensor2s), intent(IN) :: T1, T2
        
        type(ttbTensor2s) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) + T2%T(iElement)
        enddo

    end function add2s2s

!**********************************************************************************************************************
    
    function add22s(T1, T2) result(T3)

        type(ttbTensor2), intent(IN) :: T1

        type(ttbTensor2s), intent(IN) :: T2
        
        type(ttbTensor2) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) + T2%T(iElement)
        enddo

    end function add22s

!**********************************************************************************************************************

    function add2s2(T1, T2) result(T3)

        type(ttbTensor2s), intent(IN) :: T1

        type(ttbTensor2), intent(IN) :: T2
        
        type(ttbTensor2) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) + T2%T(iElement)
        enddo

    end function add2s2

!**********************************************************************************************************************

    function add4s4s(T1, T2) result(T3)

        type(ttbTensor4s), intent(IN) :: T1, T2
        
        type(ttbTensor4s) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) + T2%T(iElement)
        enddo

    end function add4s4s

!**********************************************************************************************************************

    function add44s(T1, T2) result(T3)

        type(ttbTensor4), intent(IN) :: T1
        
        type(ttbTensor4s), intent(IN) :: T2

        type(ttbTensor4) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) + T2%T(iElement)
        enddo

    end function add44s

!**********************************************************************************************************************

    function add4s4(T1, T2) result(T3)

        type(ttbTensor4s), intent(IN) :: T1
        
        type(ttbTensor4), intent(IN) :: T2

        type(ttbTensor4) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) + T2%T(iElement)
        enddo

    end function add4s4

!**********************************************************************************************************************


    function sub11(T1, T2) result(T3)

        type(ttbTensor1), intent(IN) :: T1, T2
        
        type(ttbTensor1) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) - T2%T(iElement)
        enddo

    end function sub11

!**********************************************************************************************************************

    function sub22(T1, T2) result(T3)

        type(ttbTensor2), intent(IN) :: T1, T2
        
        type(ttbTensor2) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) - T2%T(iElement)
        enddo

    end function sub22

!**********************************************************************************************************************

    function sub44(T1, T2) result(T3)

        type(ttbTensor4), intent(IN) :: T1, T2
        
        type(ttbTensor4) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) - T2%T(iElement)
        enddo

    end function sub44

!**********************************************************************************************************************

    function sub2s2s(T1, T2) result(T3)

        type(ttbTensor2s), intent(IN) :: T1, T2
        
        type(ttbTensor2s) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) - T2%T(iElement)
        enddo

    end function sub2s2s

!**********************************************************************************************************************
    
    function sub22s(T1, T2) result(T3)

        type(ttbTensor2), intent(IN) :: T1

        type(ttbTensor2s), intent(IN) :: T2
        
        type(ttbTensor2) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) - T2%T(iElement)
        enddo

    end function sub22s

!**********************************************************************************************************************

    function sub2s2(T1, T2) result(T3)

        type(ttbTensor2s), intent(IN) :: T1

        type(ttbTensor2), intent(IN) :: T2
        
        type(ttbTensor2) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) - T2%T(iElement)
        enddo

    end function sub2s2

!**********************************************************************************************************************

    function sub4s4s(T1, T2) result(T3)

        type(ttbTensor4s), intent(IN) :: T1, T2
        
        type(ttbTensor4s) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) - T2%T(iElement)
        enddo

    end function sub4s4s

!**********************************************************************************************************************

    function sub44s(T1, T2) result(T3)

        type(ttbTensor4), intent(IN) :: T1
        
        type(ttbTensor4s), intent(IN) :: T2

        type(ttbTensor4) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) - T2%T(iElement)
        enddo

    end function sub44s

!**********************************************************************************************************************

    function sub4s4(T1, T2) result(T3)

        type(ttbTensor4s), intent(IN) :: T1
        
        type(ttbTensor4), intent(IN) :: T2

        type(ttbTensor4) :: T3
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call T3%create(T1%dim)

        do iElement=1,T1%dim 
            T3%T(iElement) = T1%T(iElement) - T2%T(iElement)
        enddo

    end function sub4s4

!**********************************************************************************************************************

end module tensorAddition