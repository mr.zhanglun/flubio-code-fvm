!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	program decomposeMesh

		use meshPartitioning

		implicit none

		type(meshDecomp) :: meshDist
	!------------------------------------------------------------------------------------------------------------------

		character(len=10), dimension(:), allocatable :: args

		character(len=10) :: procid

		character(len=10) :: fileFormat
	!------------------------------------------------------------------------------------------------------------------

		integer :: nranks, nargs, irank, i

		logical :: found
	!------------------------------------------------------------------------------------------------------------------

		!------------------------!
		! command line arugments !
		!------------------------!

		nargs = command_argument_count()
		allocate(args(nargs))
		call getOptions(args, nargs, nranks, fileFormat)

		!------------------!
		! read serial mesh !
		!------------------!

		call mesh%readMeshFromFiles()
		call meshDist%readMetisMeshDecomposition(nranks)
		call meshDist%facePartitions()
		call meshDist%pointPartitions()

		write(*,*)
		write(*,*) 'FLUBIO: Start writing mesh files:'
		write(*,*)

		! Output files
		do irank = 1,nranks
			write(procid,'(i0)') irank
			write(*,*) 'Processor '//trim(procid)//' info:'
			call meshDist%writeCellProcAddressing(irank,trim(fileFormat))
			call meshDist%writeFaceProcAddressing(irank,trim(fileFormat))
			call meshDist%writeOwnerPartitions(irank,trim(fileFormat))
			call meshDist%writeNeighbourPartitions(irank,trim(fileFormat))
			call meshDist%writePointPartitions(irank,trim(fileFormat))
			call meshDist%writeFacePartitions(irank,trim(fileFormat))
			call meshDist%writeBoundaryPartitions(irank,trim(fileFormat))
			call meshDist%writeBoundaryProcAddressing(irank,trim(fileFormat))
			write(*,*)
		end do

		write(*,*) 'DONE!'
		write(*,*)

	end program decomposeMesh

! *********************************************************************************************************************

	subroutine getOptions(args, nargs, np, fileFormat)

		implicit none

		integer :: p, nargs, np, stat

		character(len=*) :: args(nargs)

		character(len=10) :: fileFormat
	!------------------------------------------------------------------------------------------------------------------

		! get the arguments
		do p=1,nargs
			call get_command_argument(number=p, value=args(p), status=stat)
		enddo

		! Default file format
		fileFormat = 'ascii'

		! process the options
		do p=1,nargs

			if(args(p)=='-np') then

				read(args(p+1),*) np

			elseif(args(p)=='-format') then	
				
				read(args(p+1),*) fileFormat

			elseif(args(p)=='-help') then

				write(*,*) 'Command line options:'
				write(*,*) '-np: number of domains'
				stop

			endif

		enddo

		if(np<1) then
			write(*,*) 'ERROR: please set the number of subdomains using -np option'
			stop
		end if

	end subroutine getOptions

! *********************************************************************************************************************
