# Compiler
F03 =  mpifort

# Flags
FFLAGS = -O3 -cpp -mcmodel=medium -fdefault-real-8 -fbounds-check -ffree-line-length-512 -ffast-math

# Directories
BINDIR = bin
OBJDIR = objs
MODS = mods

# External Libs
LIB_VTK_IO = ${flubio_libs}/VTKFortran-master/static/mpi
LIB_VTK_IO_INCLUDE = ${flubio_libs}/VTKFortran-master/static/mpi/mod_mpi/
LIB_PENF = ${flubio_libs}/VTKFortran-master/src/third_party/PENF/lib

JSON_LIB=${flubio_libs}/json-fortran/build/lib
JSON_INC=${flubio_libs}/json-fortran/build/include

QCONT_LIB = ${flubio_libs}/qcontainers/build/lib

LDFLAGS = -L$(LIB_VTK_IO) -lvtkfortran_mpi -L$(LIB_PENF) -lpenf -L$(JSON_LIB) -ljsonfortran -L$(QCONT_LIB) -lqcontainers
INCLUDE = -I$(LIB_VTK_IO_INCLUDE) -I$(JSON_INC)

### DO NOT CHANGE BELOW THIS LINE ####

# Linked libraries

# Post-processor
VPATH =  ../../mpi ../include/qcontainers_f ../orderPack/ ../include/ ../common/ formats/ mesh/ postProc/ preProc/ \
        postProc/sample

OBJ_POST = $(addprefix $(OBJDIR)/, flubioFields_parallel.o globalMeshVar.o flubioMpi.o  auxiliaries.o checkArgs.o \
             unirnk.o uniinv.o unista.o refsor.o generalDictionaries.o qlibc_util.o qvector.o math.o \
             nodes.o faces.o elements.o flubioBoundaries.o flubioParallelMesh.o meshvar.o mstrings.o userDefinedTypes.o \
             vtkFormat.o kdtree2.o flubioFields.o probes.o lineProbes.o sample.o)

OBJ_MESH = $(addprefix $(OBJDIR)/, meshView_parallel.o globalMeshVar.o flubioMpi.o  auxiliaries.o checkArgs.o \
             unirnk.o uniinv.o unista.o refsor.o generalDictionaries.o math.o \
             nodes.o faces.o elements.o flubioBoundaries.o flubioParallelMesh.o meshvar.o mstrings.o userDefinedTypes.o \
             vtkFormat.o kdtree2.o)

OBJ_BMESH = $(addprefix $(OBJDIR)/, boundaryMesh_parallel.o globalMeshVar.o flubioMpi.o  auxiliaries.o checkArgs.o \
             unirnk.o uniinv.o unista.o refsor.o generalDictionaries.o math.o \
             nodes.o faces.o elements.o flubioBoundaries.o flubioParallelMesh.o meshvar.o mstrings.o userDefinedTypes.o \
             vtkFormat.o kdtree2.o)

OBJ_BOUND = $(addprefix $(OBJDIR)/, flubioBoundaryFields_parallel.o globalMeshVar.o flubioMpi.o  auxiliaries.o checkArgs.o \
             unirnk.o uniinv.o unista.o refsor.o generalDictionaries.o math.o \
             nodes.o faces.o elements.o flubioBoundaries.o flubioParallelMesh.o meshvar.o mstrings.o userDefinedTypes.o \
             vtkFormat.o kdtree2.o)

OBJ_SAMPLE = $(addprefix $(OBJDIR)/, sample.o flubio_sampling.o globalMeshVar.o flubioMpi.o generalDictionaries.o math.o\
             auxiliaries.o checkArgs.o generalDictionaries.o qlibc_util.o qvector.o unista.o uniinv.o unirnk.o refsor.o \
             nodes.o faces.o elements.o flubioBoundaries.o flubioParallelMesh.o meshvar.o mstrings.o userDefinedTypes.o \
             vtkFormat.o flubioFields.o kdtree2.o probes.o lineProbes.o sample.o)

# Build options
default: flubio_fields_parallel flubio_meshview_parallel flubio_boundaryMesh_parallel flubio_boundaryFields_parallel \
         flubio_sample

meshview: flubio_meshview_parallel
boundary_mesh: flubio_boundaryMesh_parallel
fields: flubio_fields_parallel
boundaryFields: flubio_boundaryFields_parallel
sample: flubio_sample

# Cleaning rules
clean:
	rm -f $(BINDIR)/*

sclean:
	rm -f $(OBJDIR)/*.o $(MODS)/*.mod $(BINDIR)/*

distclean: sclean
	rm -f *~ */*~
	rm -r mods objs

# Executables
flubio_fields_parallel: $(BINDIR)/flubio_fields_parallel
flubio_meshview_parallel: $(BINDIR)/flubio_meshview_parallel
flubio_boundaryMesh_parallel: $(BINDIR)/flubio_boundaryMesh_parallel
flubio_boundaryFields_parallel: $(BINDIR)/flubio_boundaryFields_parallel
flubio_sample: $(BINDIR)/flubio_sample

# Compile source files

.SUFFIXES: .f03
$(OBJDIR)/%.o: %.f03
	@echo
	@mkdir -p objs mods bin
	$(F03) $(FFLAGS) $(LDFLAGS) $(INCLUDE) -J$(MODS) -c -o $@ $<

# Build tools
$(BINDIR)/flubio_fields_parallel: $(OBJ_POST)
	@echo
	@echo --- Building flubio_fields ---
	@echo
	$(F03) $^ $(FFLAGS) $(LDFLAGS) $(INCLUDE) -o $@
	@echo
	@echo flubio_fields built
	@echo

$(BINDIR)/flubio_meshview_parallel: $(OBJ_MESH)
	@echo
	@echo --- Building flubio_meshview ---
	@echo
	$(F03) $^ $(FFLAGS) $(LDFLAGS) $(INCLUDE) -o $@
	@echo
	@echo flubio_meshview built
	@echo

$(BINDIR)/flubio_boundaryMesh_parallel: $(OBJ_BMESH)
	@echo
	@echo --- Building flubio_boundaryMesh_parallel ---
	@echo
	$(F03) $^ $(FFLAGS) $(LDFLAGS) $(INCLUDE) -o $@
	@echo
	@echo flubio_boundaryMesh_parallel built
	@echo

$(BINDIR)/flubio_boundaryFields_parallel: $(OBJ_BOUND)
	@echo
	@echo --- Building flubio_boundaryFields_parallel ---
	@echo
	$(F03) $^ $(FFLAGS) $(LDFLAGS) $(INCLUDE) -o $@
	@echo
	@echo flubio_boundaryFields_parallel built
	@echo

$(BINDIR)/flubio_sample: $(OBJ_SAMPLE)
	@echo
	@echo --- Building flubio_sample ---
	@echo
	$(F03) $^ $(FFLAGS) $(LDFLAGS) $(INCLUDE) -o $@
	@echo
	@echo flubio_sample built
	@echo

# Dependencies
$(OBJDIR)/generalDictionaries.o: $(addprefix $(OBJDIR)/, mstrings.o)
$(OBJDIR)/meshvar.o: $(addprefix $(OBJDIR)/, globalMeshVar.o flubioParallelMesh.o)
$(OBJDIR)/meshvar.o: $(addprefix $(OBJDIR)/, globalMeshVar.o flubioParallelMesh.o)
$(OBJDIR)/nodes.o: $(addprefix $(OBJDIR)/, globalMeshVar.o userDefinedTypes.o)
$(OBJDIR)/flubioBoundaries.o: $(addprefix $(OBJDIR)/, globalMeshVar.o flubioMpi.o mstrings.o)
$(OBJDIR)/faces.o: $(addprefix $(OBJDIR)/, globalMeshVar.o nodes.o userDefinedTypes.o)
$(OBJDIR)/elements.o: $(addprefix $(OBJDIR)/, globalMeshVar.o faces.o userDefinedTypes.o)
$(OBJDIR)/flubioParallelMesh.o: $(addprefix $(OBJDIR)/, globalMeshVar.o auxiliaries.o flubioBoundaries.o elements.o math.o unirnk.o refsor.o unista.o userDefinedTypes.o kdtree2.o)
$(OBJDIR)/vtkFormat.o: $(addprefix $(OBJDIR)/, meshvar.o)
$(OBJDIR)/unista.o: $(addprefix $(OBJDIR)/, uniinv.o)
$(OBJDIR)/unista.o: $(addprefix $(OBJDIR)/, uniinv.o)
$(OBJDIR)/flubioFields_parallel.o: $(addprefix $(OBJDIR)/, meshvar.o)
$(OBJDIR)/extractBlock.o: $(addprefix $(OBJDIR)/, meshvar.o kdtree2.o mstrings.o)
$(OBJDIR)/qvector.o: $(addprefix $(OBJDIR)/, qlibc_util.o)
$(OBJDIR)/sample.o: $(addprefix $(OBJDIR)/, meshvar.o kdtree2.o mstrings.o generalDictionaries.o probes.o lineProbes.o)
$(OBJDIR)/flubio_sample.o: $(addprefix $(OBJDIR)/, meshvar.o kdtree2.o mstrings.o generalDictionaries.o sample.o)