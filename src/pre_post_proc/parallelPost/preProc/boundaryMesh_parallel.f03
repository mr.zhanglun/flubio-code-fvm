!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

    program flubioBoundaryMesh

        use meshvar
        use mpi
        use m_strings
        use parallelVTKFormat, only: processBoundaryMeshVTK

        implicit none

        character(len=10) :: fmt
        !! output format

        character(len=500) :: boundary
        !! boundary list as comma separated string

        character(len=50), dimension(:), allocatable :: boundaryList
        !!list of boundaries to process

        character(len=500), dimension(:), allocatable :: args
        !! arguments
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary
        !! target boundary

        integer :: nargs
        !! number of command line arguments
                
        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        call mpi_init(ierr)
        
        ! Initialize MPI variables
        call setMPIvar()

        !------------------------!
        ! Command line arguments !
        !------------------------!

        nargs = command_argument_count()

        if(nargs>0) then
            allocate(args(nargs))
            call parseCommandLine(args, nargs, boundary, fmt)
            call split_in_array(boundary, boundaryList, ',')
        else
            call flubioStopMsg("ERROR: Please specify all the command line options. Run with -help for more info.")
        endif

        !------------------!
        ! read serial mesh !
        !------------------!

        call mesh%readMeshFromFiles()

        !------------------!
        ! Write data files !
        !------------------!

        do iBoundary=1,size(boundaryList)
            call processBoundaryMeshVTK(trim(boundaryList(iBoundary)), fmt)
        end do

        call mpi_finalize(ierr)

    end program flubioBoundaryMesh

! *********************************************************************************************************************

    subroutine parseCommandLine(args, nargs, boundary, fmt)

        use flubioMpi

        implicit none

        character(len=10) :: fmt

        character(len=*) :: args(nargs), boundary
    !--------------------------------------------------------------------------------------------------------------

        integer :: p, nargs, stat
    !--------------------------------------------------------------------------------------------------------------

        logical :: found
    !--------------------------------------------------------------------------------------------------------------

        ! Get the arguments
        do p=1,nargs
            call get_command_argument(number=p, value=args(p), status=stat)
        enddo

        fmt = 'binary'

        ! Proces the options
        do p=1,nargs

            if(args(p)=='-boundary') then

                boundary = args(p+1)
                found = .true.

            elseif(args(p)=='-format') then

                fmt = trim(args(p+1))
                
            elseif(args(p)=='-help') then

                call flubioMsg('Command line options:')
                call flubioMsg('-boundary: target boundary to process. List of boundaries are comma separeted with no spaces')

                call flubioMsg('')
                call flubioMsg('example: mpirun -np 4 flubio_boundaryMesh_parallel -boundary b1,b2,b3')

                call flubioStopMsg('')

            endif

        enddo

        if(.not. found) call flubioStopMsg('FLUBIO: missing argument "boundary". Specify the boundary to process')

    end subroutine parseCommandLine

! *********************************************************************************************************************
