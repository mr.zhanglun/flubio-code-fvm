module lineProbes

    use flubioMpi
    use probes, only: probeObj

    implicit none

    ! Extend for line probes
    type, extends(probeObj), public :: lineProbeObj
        real :: startPoint(3)
        real :: endPoint(3)
    contains
        procedure :: createProbePoints
    end type lineProbeObj

contains

    subroutine createProbePoints(this)

    !==================================================================================================================
    ! Description:
    !! createProbePoints generates the cartisian ponts along the segment joining the inout points.
    !==================================================================================================================

        class(lineProbeObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: n, iTime
    !------------------------------------------------------------------------------------------------------------------

        real :: t(this%nProbes)
    !------------------------------------------------------------------------------------------------------------------

        t = linspace(0.0, 1.0, this%nProbes)

        this%x(1) = this%startPoint(1)
        this%y(1) = this%startPoint(2)
        this%z(1) = this%startPoint(3)

        this%x(this%nProbes) = this%endPoint(1)
        this%y(this%nProbes) = this%endPoint(2)
        this%z(this%nProbes) = this%endPoint(3)

        do n=2,this%nProbes-1
            this%x(n) = this%x(1) + t(n)*(this%x(this%nProbes)-this%x(1))
            this%y(n) = this%y(1) + t(n)*(this%y(this%nProbes)-this%y(1))
            this%z(n) = this%z(1) + t(n)*(this%z(this%nProbes)-this%z(1))
        end do

    end subroutine createProbePoints

! *********************************************************************************************************************

    function linspace(a, b, N)  result(x)

        implicit none

        integer :: i, N
   !--------------------------------------------------------------------------------------------------------------------

        real :: a, b, x(N)
    !--------------------------------------------------------------------------------------------------------------------

        if(N==1) then

            x(1) = a

        else

            do i = 1, N
                x(i) = a + (b-a)*real(i-1)/real(N-1)
            end do

        end if

    end function linspace

! *********************************************************************************************************************


end module lineProbes