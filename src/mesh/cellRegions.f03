!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module cellRegions

    use globalTimeVar
    use globalMeshVar
    use flubioMpi
    use math
    use fossil
    use vecfor, only: ex_R8P, ey_R8P, ez_R8P

    implicit none

    type :: box

        real :: center(3)
        !! box center

        real :: lenghts(3)
        !! lenght in the three directions

        contains
            procedure :: createBox    
            procedure :: isInside => isInsideBox
    end type box

    type :: cylinder

        real :: bottomCapCenter(3)
        !! bottom cap face center

        real :: topCapCenter(3)
        !! top cap face center

        real :: radius
        !! cylinder radius

        real :: lengthsqr
        !! cylinder length squared

        real :: d(3)
        !! cylidner length vector

        contains
            procedure :: createCylinder
            procedure :: isInside => isInsideCylinder

    end type cylinder

    type :: sphere

        real :: center(3)
        !! center

        real :: radius
        !! radius

        contains
            procedure :: createSphere
            procedure :: isInside => isInsideSphere

    end type sphere

    type :: cone

        real :: center(3)
        !! base center

        real :: baseNormal(3)
        !! normal to orient the cone in space
        
        real :: apex(3)
        !! apex coordinates

        real :: axis(3)
        !! cone axis

        real :: h
        !! cone height

        real :: radius
        !! base radius

        real :: alpha
        !! cone aperture

    contains
        procedure :: createCone
        procedure :: isInside => isInsideCone

    end type cone

    type :: stl

        character(len=:), allocatable:: fileName
        !! stl file path/name

        type(file_stl_object)    :: STLFile
        !! STL file object

        type(surface_stl_object) :: STLSurface
        !! STL surface object

        logical :: scale, translate, rotateByAxis

        real :: axisAndAngle(4)
        !! array containing the axis of rotation

        !real :: rotateByMatrix(3,3)
        !! rotation matrix

        real :: delta(3)
        !! translation vector

        real :: scalingFactor(3)
        !! scalinf factor along axis

    contains

        procedure :: isInside => isInsideSTL

    end type stl

    type :: region

        character(len=:), allocatable :: regionName
        !! Region name

        character(len=:), allocatable :: regionType
        ! Region type

        real, dimension(:,:), allocatable :: phi
        !! Region position

        type(box) :: regionBox
        !! Region box

        type(cylinder) :: regionCylinder
        !! Region cylinder

        type(sphere) :: regionSphere
        !! Region sphere

        type(cone) :: regionCone
        !! Region cone

        type(stl) :: regionSTL
        !! Region STL

    contains
        procedure :: createBoxRegion
        procedure :: createCylindricalRegion
        procedure :: createSphericalRegion
        procedure :: createConicalRegion
        procedure :: createSTLRegion
        procedure :: isInsideRegion
        procedure :: writeRegionToFile

    end type region
    
contains

    subroutine createBox(this, center, lenghts)

        class(box) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: center(3)
        !! box center

        real :: lenghts(3)
        !! length of the box in the cartesian directions
    !------------------------------------------------------------------------------------------------------------------

        this%center = center
        this%lenghts = lenghts

    end subroutine createBox

!**********************************************************************************************************************

    subroutine createCylinder(this, p1, p2, r)

        class(cylinder) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: p1(3)
        !! bottom cap face center

        real :: p2(3)
        !! top cap face center

        real :: r
        !! cylinder radius
    !------------------------------------------------------------------------------------------------------------------

        this%bottomCapCenter = p1
        this%topCapCenter = p2
        this%radius = r

        this%d = p2-p1
        this%lengthsqr = this%d(1)**2 + this%d(2)**2 + this%d(3)**2

    end subroutine createCylinder

!**********************************************************************************************************************

    subroutine createSphere(this, center, radius)

        class(sphere) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: radius
        !! Sphere radius

        real :: center(3)
        !! sphere center
    !------------------------------------------------------------------------------------------------------------------

        this%center = center
        this%radius = radius

    end subroutine createSphere

!**********************************************************************************************************************

    subroutine createCone(this, center, baseNormal, radius, h)

        class(cone) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: radius
        !! cone base radius

        real :: center(3)
        !! cone center

        real :: baseNormal(3)
        !! base normal

        real :: h
        !! cone height

        real :: magBaseNormal
    !------------------------------------------------------------------------------------------------------------------

        this%center = center
        this%radius = radius
        this%h = h

        this%baseNormal = baseNormal

        this%alpha = atan(h/radius)

        magBaseNormal = this%baseNormal(1)**2 + this%baseNormal(2)**2 + this%baseNormal(3)**2 + 1e-12
        this%apex = this%center + h*this%baseNormal/sqrt(magBaseNormal)
        this%axis = this%apex - center

    end subroutine createCone

!**********************************************************************************************************************

    function isInsideBox(this, point) result(inside)

        class(box) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! inside flag

        integer :: inside
        !! inside flag
    !------------------------------------------------------------------------------------------------------------------

        real :: point(3)
        !! test point

        real :: half(3)
        !! half of the lenghts in the cartesian direction

        real :: pcd(3)
        !! point to center distance

        real :: dot(3)
        !! dot products

        real :: dx(3)
        !! x direction versor

        real :: dy(3)
        !! y direction versor

        real :: dz(3)
        !! z direction versor
    !------------------------------------------------------------------------------------------------------------------

        dx = (/1.0, 0.0, 0.0 /)
        dy = (/0.0, 1.0, 0.0 /)
        dz = (/0.0, 0.0, 1.0 /)

        half = 0.5*this%lenghts
        pcd = point - this%center;

        dot(1) = abs(dot_product(pcd, dx))
        dot(2) = abs(dot_product(pcd, dy))
        dot(3) = abs(dot_product(pcd, dz))

        if(dot(1)<=half(1) .and. dot(2)<=half(2) .and. dot(3)<=half(3)) then
            inside = 1
        else
            inside = 0
        end if

    end function isInsideBox

!**********************************************************************************************************************

    function isInsideCylinder(this, p) result(inside)

        class(cylinder) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: inside
    !------------------------------------------------------------------------------------------------------------------

        real :: p(3)
        !! test point

        real :: pd(3)
        !! point1 to test point

        real :: dot, magpd
        !! dot product

        real :: dsq
        !! distance to the axis squared
    !------------------------------------------------------------------------------------------------------------------

        ! test point to p1
        pd = p-this%bottomCapCenter

        magpd = dot_product(pd, pd)
        dot = dot_product(pd, this%d)

        ! Check if the points is inside the caps' bounds
        if(dot < 0.0 .or. dot > this%lengthsqr) then
            inside = 0

        ! If it is inside the caps bounds, then check if the distance to the axis is less then the radius
        else

            dsq = magpd - dot*dot/this%lengthsqr;

            if(dsq > this%radius**2 ) then
                inside = 0
            else
                inside = 1
            end if

        end if

    end function isInsideCylinder

!**********************************************************************************************************************

    function isInsideSphere(this, p) result(inside)

        class(sphere) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: inside
    !------------------------------------------------------------------------------------------------------------------

        real :: p(3)
        !! test point

        real :: pc(3)
        !! point to center vector

        real :: magpc
    !------------------------------------------------------------------------------------------------------------------

        pc = p - this%center

        magpc = dot_product(pc, pc)

        if(magpc <= this%radius**2) then
            inside = 1
        else
            inside = 0
        end if

    end function isInsideSphere
    
!**********************************************************************************************************************

    function isInsideCone(this, p) result(inside)

        class(cone) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: inside
    !------------------------------------------------------------------------------------------------------------------

        real :: p(3)
        !! test point

        real :: ap(3)
        !! point to center vector

        real :: magap, dot, dot1
    !------------------------------------------------------------------------------------------------------------------

        ap = this%apex - p
        call mag(this%apex, magap)

        dot = dot_product(ap/magap, this%axis/this%h)

        if(dot > cos(this%alpha)) then
            inside = 0
        else

            dot1 = dot_product(ap, this%axis/this%h)

            if(dot1<this%h) then
                inside = 1
            else
                inside = 0
            end if

        end if

    end function isInsideCone

! *********************************************************************************************************************

    function isInsideSTL(this, p) result(inside)

        class(stl) :: this
    !------------------------------------------------------------------------------------------------------------------

        logical :: isInside
    !------------------------------------------------------------------------------------------------------------------

        integer :: inside
    !------------------------------------------------------------------------------------------------------------------

        real :: p(3)
    !------------------------------------------------------------------------------------------------------------------

        isInside = this%STLSurface%is_point_inside_polyhedron_ri(point=p(1)*ex_R8P+p(2)*ey_R8P+p(3)*ez_R8P)

        if(isInside) then
            inside = 1
        else
            inside = 0
        end if

    end function isInsideSTL

! *********************************************************************************************************************

    subroutine createBoxRegion(this, regionName, regionType, center, dimensions)
        
    !==================================================================================================================
    ! Description:
    !! createRegion is the region constructor and it allocates the memory for a new region.
    !==================================================================================================================
    
        class(region) :: this
    !-------------------------------------------------------------------------------------------------------------------

        character(*) :: regionName, regionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iFace, inside
    !------------------------------------------------------------------------------------------------------------------

        real :: center(3), dimensions(3), point(3)
    !------------------------------------------------------------------------------------------------------------------

        this%regionName = regionName
        this%regionType = regionType

        call this%regionBox%createBox(center, dimensions)

        allocate(this%phi(numberOfElements+numberOfBfaces,1))
        this%phi = 0.0

    end subroutine createBoxRegion

! *********************************************************************************************************************

    subroutine createCylindricalRegion(this, regionName, regionType, p1, p2, r)

    !==================================================================================================================
    ! Description:
    !! createCylindricalRegion is the region constructor and it allocates the memory for a new region.
    !==================================================================================================================

        class(region) :: this
    !-------------------------------------------------------------------------------------------------------------------

        character(*) :: regionName, regionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: p1(3), p2(3)

        real :: r
    !------------------------------------------------------------------------------------------------------------------

        this%regionName = regionName
        this%regionType = regionType

        call this%regionCylinder%createCylinder(p1, p2, r)

        allocate(this%phi(numberOfElements+numberOfBfaces,1))
        this%phi = 0.0

    end subroutine createCylindricalRegion

! *********************************************************************************************************************

    subroutine createSphericalRegion(this, regionName, regionType, center, radius)

    !==================================================================================================================
    ! Description:
    !! createSphericalRegion is the region constructor and it allocates the memory for a new region.
    !==================================================================================================================

        class(region) :: this
    !-------------------------------------------------------------------------------------------------------------------

        character(*) :: regionName, regionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: center(3)

        real :: radius
    !------------------------------------------------------------------------------------------------------------------

        this%regionName = regionName
        this%regionType = regionType

        call this%regionSphere%createSphere(center, radius)

        allocate(this%phi(numberOfElements+numberOfBfaces,1))
        this%phi = 0.0

    end subroutine createSphericalRegion
    
! *********************************************************************************************************************

    subroutine createConicalRegion(this, regionName, regionType, center, baseNormal, radius, h)
        
    !==================================================================================================================
    ! Description:
    !! createConicalRegion is the region constructor and it allocates the memory for a new region.
    !==================================================================================================================
        
        class(region) :: this
    !-------------------------------------------------------------------------------------------------------------------

        character(*) :: regionName, regionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: center(3)

        real :: baseNormal(3)
        
        real :: radius
        
        real :: h
    !------------------------------------------------------------------------------------------------------------------

        this%regionName = regionName
        this%regionType = regionType

        call this%regionCone%createCone(center, baseNormal, radius, h)

        allocate(this%phi(numberOfElements+numberOfBfaces,1))
        this%phi = 0.0

    end subroutine createConicalRegion

! *********************************************************************************************************************

    subroutine createSTLRegion(this, regionName, regionType, stlFileName, &
                                                             stlTransformations, scale, delta, rotateByAxis)

    !==================================================================================================================
    ! Description:
    !! createSTLRegion is the region constructor and it allocates the memory for a new region.
    !==================================================================================================================

        class(region) :: this
    !-------------------------------------------------------------------------------------------------------------------

        character(*) :: regionName, regionType, stlFileName
    !------------------------------------------------------------------------------------------------------------------

        logical :: stlTransformations(4)
    !------------------------------------------------------------------------------------------------------------------

        real :: scale(3), delta(3), rotateByAxis(4)
    !------------------------------------------------------------------------------------------------------------------

        this%regionName = regionName
        this%regionType = regionType
        this%regionSTL%fileName = stlFileName

        this%regionSTL%scale = stlTransformations(1)
        this%regionSTL%translate = stlTransformations(2)
        this%regionSTL%rotateByAxis = stlTransformations(3)

        call this%regionSTL%STLFile%load_from_file(facet=this%regionSTL%STLSurface%facet, file_name=stlFileName, &
                                                   is_ascii=.true.)

        !call this%regionSTL%STLSurface%sanitize_normals()
        call this%regionSTL%STLSurface%analize()

        if(this%regionSTL%scale) then
           this%regionSTL%scalingFactor = scale
           call this%regionSTL%STLSurface%resize(factor=scale(1)*ex_R8P + scale(2)*ey_R8P + scale(3)*ez_R8P, &
                                                 recompute_metrix=.True.)
        endif

        if(this%regionSTL%translate) then
            this%regionSTL%delta = delta
            call this%regionSTL%STLSurface%translate(x=delta(1), y=delta(2), z=delta(3), recompute_metrix=.True.)
        endif

        if(this%regionSTL%rotateByAxis) then
            this%regionSTL%axisAndAngle = rotateByAxis
            call this%regionSTL%STLSurface%rotate(axis=rotateByAxis(1)*ex_R8P + &
                                                       rotateByAxis(2)*ey_R8P + &
                                                       rotateByAxis(3)*ez_R8P, &
                                                       angle=rotateByAxis(4), &
                                                  recompute_metrix=.True.)
        endif

        allocate(this%phi(numberOfElements+numberOfBfaces,1))
        this%phi = 0.0

    end subroutine createSTLRegion

! *********************************************************************************************************************

    function isInsideRegion(this, point) result(inside)

    !==================================================================================================================
    ! Description:
    !! createSphericalRegion is the region constructor and it allocates the memory for a new region.
    !==================================================================================================================

        class(region) :: this
    !-------------------------------------------------------------------------------------------------------------------

        integer :: inside
   !------------------------------------------------------------------------------------------------------------------

        real :: point(3)
    !------------------------------------------------------------------------------------------------------------------

        if(this%regionType=='box') then
            inside = this%regionBox%isInside(point)
        elseif(this%regionType=='cylinder') then
            inside = this%regionCylinder%isInside(point)
        elseif(this%regionType=='sphere') then
            inside = this%regionSphere%isInside(point)
        elseif(this%regionType=='cone') then
            inside = this%regionCone%isInside(point)
        elseif(this%regionType=='stl') then
            inside = this%regionSTL%isInside(point)
        else
            call flubioStopMsg('ERROR: unknown or missing region type. Please choose one among box, cylinder, sphere')
        end if

    end function isInsideRegion

!**********************************************************************************************************************
    
    subroutine writeRegionToFile(this)
        
    !==================================================================================================================
    ! Description:
    !! writeRegionToFile writes the region to a file to be post processed.
    !==================================================================================================================
        
        class(region) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=20) :: timeStamp
        !! time stamp

        character(len=20) ::  procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
        !! boolean to check if the directory exists
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target components
    !------------------------------------------------------------------------------------------------------------------

        iComp = 1
        
        write(procID,'(i0)') id

        postDir='postProc/fields/'
        write(timeStamp,'(i0)') itime
        fieldDir = postDir//trim(timeStamp)//'/'

        ! Create a directory for the field. If it does not exists create it.
        inquire(file=trim(fieldDir)//'/.', exist=dirExists)

        if(dirExists) then

            ! write Field
            open(1,file=fieldDir//trim(this%regionName)//'-d'//trim(procID)//'.bin', form='unformatted')
    
                write(1) iComp
                write(1) this%regionName

                write(1) this%phi
                write(1) iComp
                write(1) iComp

            close(1)

        elseif(.not. dirExists) then

            ! create the folder
            call execute_command_line ('mkdir -p ' // adjustl(trim(fieldDir) ) )

            ! write Field
            open(1,file=fieldDir//trim(this%regionName)//'-d'//trim(procID)//'.bin', form='unformatted')

                write(1) iComp
                write(1) this%regionName
            
                write(1) this%phi
                write(1) iComp
                write(1) iComp
                    
            close(1)

        end if

    end subroutine writeRegionToFile
    
end module cellRegions