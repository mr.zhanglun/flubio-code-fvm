!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module faces

!==================================================================================================================
! Description:
!! This module implements contains the data structures and methods for the mesh faces.
!==================================================================================================================

    use globalMeshVar
    use flubioMpi
    use nodes
    use userDefininedTypes

    implicit none

    type, public, extends(grid_nodes) ::  grid_faces

        integer :: numberOfFaces
        !! Number of faces in the mesh. Same as numberOfFaces

        integer :: maxFaces
        !! Maxium number of faces composing grid elements.
        !! It is arbitrary set to 6, but it can be changed if required.

        integer, dimension(:), allocatable :: numberOfFaceNodes
        !!  Number vertices composing each face

        type(integerCellStruct), dimension(:), allocatable :: fvertex
        !! Structure containing the vertex indeces composing each face

        integer, dimension(:), allocatable :: owner
        !! Array containing the list of owner cells

        integer, dimension(:), allocatable :: neighbour
        !! Array containing the list of neighbour cells

        !---------------------!
        ! Geometric varaibles !
        !---------------------!

        real, dimension(:,:), allocatable :: fcentroid
        !! Cartesian coordinates of the face center

        real, dimension(:,:), allocatable :: Sf
        !! Face normal pointing from owner to neighbour cell. The vector magnitude is the face area

        real, dimension(:,:), allocatable :: nf
        !! Face normal pointing from owner to neighbour. The vactor magniute is one

        real, dimension(:), allocatable :: area
        !! Face area. Same as mag(Sf)

        real, dimension(:,:), allocatable :: CN
        !! Distance vector, joining owner cell center with neighbour cell center

        real, dimension(:,:), allocatable :: eCN
        !! Versor  joining owner cell center with neighbour cell center (i.e. CN/|CN|)

        real, dimension(:), allocatable :: gDiff
        !! Diffusion weighting factor, abs(Sf)/abs(CN)

        real, dimension(:), allocatable :: gf
        !! Linear interpolation weighting factor

        real, dimension(:,:), allocatable :: Tf
        !! Non-orthogonality vector, i.e. Sf-Ef

        real, dimension(:,:), allocatable :: Ef
        !! Vector joining owner cell center with neighbour cell center. It definitaion is approach dependent

        real, dimension(:), allocatable :: wallDist
        !! Reciprocal of gDiff, i.e. 1/gDiff

        real, dimension(:), allocatable :: faceSign
        !! Array needed during the cell volume calculation. It is +1 if the face belongs to the owner cell, -1 otherwise

        integer, dimension(:), allocatable :: iOwnerNeighbourCoef
        !! Index used during coefficient assembling

        integer, dimension(:), allocatable :: iNeighbourOwnerCoef
        !! Index used during coefficient assembling

        ! pressure Ef and Tf

        real, dimension(:,:), allocatable :: Tfp
        !! Non-orthogonality vector for the pressure equation diffusion term

        real, dimension(:,:), allocatable :: Efp
        !! Vector needed for the pressure diffuive fluxes

        ! Face center for skewness correction

        real, dimension(:), allocatable :: skw_gf
        !! Skew-corrected interpolation weighting factor

        real, dimension(:,:), allocatable :: fcentroid_skw
        !! Skew-corrected face centers

    contains
        procedure :: readMeshFaces
    end type

contains

    subroutine readMeshFaces(this)

    !==================================================================================================================
    ! Description:
    !!  readMeshFaces reads the faces composing the mesh from the mesh files.
    !==================================================================================================================

        class(grid_faces) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10) :: procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace
        !! target mesh face

        integer :: n
        !! loop index

        integer :: nf
        !! number of faces

        integer :: nn
        !! number of neighbours

        integer:: no
        !! number of owners

        integer :: dummy
        !! dummy integer

        integer :: max_owner
        !! max owner index

        integer :: max_neighbour
        !! max neighbour index

        integer :: max_faces
        !! max number of faces
    !------------------------------------------------------------------------------------------------------------------

        real :: dummy2
        !! dummy real
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        !------------!
        ! Read Faces !
        !------------!

        open(1,file='grid/processor'//trim(procID)//'/FLUBIO_faces')

            read(1,*) nf
            allocate(numberOfFaceNodes(nf))
            allocate(this%fvertex(nf))

            do n=1,nf
                read(1,*) numberOfFaceNodes(n)
                call this%fvertex(n)%initIntColumn(numberOfFaceNodes(n), -1)
            enddo

        close(1)

        ! Set sizes, very important
        max_faces = maxval(numberOfFaceNodes)
        this%NumberOfFaces = nf
        numberOfFaces = nf

        ! Read faces from mesh file, EXP wtih the struct
        open(1,file='grid/processor'//trim(procID)//'/FLUBIO_faces')

            read(1,*)

            do n=1,nf
                read(1,*) dummy, this%fvertex(n)%col(1:numberOfFaceNodes(n))
                ! Add 1 since vertices numbering start from 1 and not from 0
                this%fvertex(n)%col(1:numberOfFaceNodes(n)) = this%fvertex(n)%col(1:numberOfFaceNodes(n)) + 1
            enddo

        close(1)

        !--------------------------!
        ! Read Owner and Neighbour !
        !--------------------------!

        open(1,file='grid/processor'//trim(procID)//'/FLUBIO_owner')

            read(1,*) no
            allocate(this%owner(no))

            do n=1,no
                read(1,*) this%owner(n)
            enddo

        close(1)
        this%owner=this%owner+1

        open(1,file='grid/processor'//trim(procID)//'/FLUBIO_neighbour')

            read(1,*) nn
            allocate(this%neighbour(this%NumberOfFaces))

            this%neighbour=0
            do n=1,nn
                read(1,*) this%neighbour(n)
            enddo

        close(1)

        ! Set sizes, very important
        this%neighbour = this%neighbour+1
        numberOfIntFaces =  nn
        max_owner = maxval(this%owner)
        max_neighbour = maxval(this%neighbour)
        numberOfElements = max0(max_owner, max_neighbour)
        maxFaces = 6

        ! Read faceProcAddressing
        open(1, file='grid/processor'//trim(procID)//'/FLUBIO_faceProcAddressing')

            read(1,*) nf
            allocate(this%faceSign(nf))

            do iFace=1, nf
                read(1,*) dummy2
                this%faceSign(iFace) = dummy2/abs(dummy2)
            enddo

        close(1)
    end subroutine readMeshFaces

end module faces