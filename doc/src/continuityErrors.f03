module continuityErrors

    use flubioDictionaries, only: flubioMonitors, raiseKeyErrorJSON
    use globalMeshVar
    use globalTimeVar
    use fieldvar, only: mf
    use flubioMpi
    use operators
    use json_module
    use runTimeObject

    implicit none

    type, public, extends(runTimeObj) :: contErr

        character(len=30) :: fieldName
        !! Name of the field

        integer :: nComp
        !! number of components

        real :: maxLocalErr
        
        real :: maxGlobalErr
        
        real, dimension(:,:), allocatable :: phi
        !! field variable

    contains

        procedure :: initialise => createContErr
        procedure :: run => computeContErr
        procedure :: writeFieldFunction

    end type contErr

contains
    
    subroutine createContErr(this)

    !==================================================================================================================
    ! Description:
    !! createContErr is the field constructor and it allocates the memory for a new flubioField.
    !==================================================================================================================

        class(contErr) :: this
    !-------------------------------------------------------------------------------------------------------------------

        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)

        ! Get monitor pointer
        call jcore%get_child(monitorsPointer, this%objName, monitorPointer, monitorFound)

        call jcore%get(monitorPointer, 'saveEvery', this%runAt, found)
        call raiseKeyErrorJSON('saveEvery', found)

        this%fieldName = 'contErr'
        this%nComp = 1
        allocate(this%phi(numberOfElements+numberOfBFaces,this%nComp))

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()

    end subroutine createContErr

! *********************************************************************************************************************

    subroutine computeContErr(this)

    !==================================================================================================================
    ! Description:
    !! computeContErr computes the total pressure as:
    !! \begin{equation*}
    !! P_t= p + 0.5\rho|U|^2
    !! \end{equation*}
    !==================================================================================================================

        class(contErr):: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i
    !------------------------------------------------------------------------------------------------------------------

        real :: u, v, w, p
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%runAt)==0) then
            this%phi = 0.0
            this%phi(1:numberOfElements,1) = div(mf)
            call this%writeFieldFunction()
        endif

    end subroutine computeContErr

!**********************************************************************************************************************

    subroutine writeFieldFunction(this)

    !==================================================================================================================
    ! Description:
    !! writeToFile writes the field to a file to be post processed.
    !==================================================================================================================

        class(contErr) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=20) :: timeStamp
        !! time stamp

        character(len=20) ::  procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
        !! boolean to check if the directory exists
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target components
   !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        postDir='postProc/fields/'
        write(timeStamp,'(i0)') itime
        fieldDir = postDir//trim(timeStamp)//'/'

        ! Create a directory for the field. If it does not exists create it.

        inquire(file=trim(fieldDir)//'/.', exist=dirExists)

        if(dirExists) then

            ! write Field
            open(1,file=fieldDir//trim(this%fieldName)//'-d'//trim(procID)//'.bin', form='unformatted')

            write(1) this%nComp
            write(1) this%fieldName

            do iComp = 1,this%nComp
                write(1) this%phi(:,iComp)
                write(1) iComp
                write(1) iComp
            end do

            close(1)

        elseif(.not. dirExists) then

            ! create the folder
            call execute_command_line ('mkdir -p ' // adjustl(trim(fieldDir) ) )

            ! write Field
            open(1,file=fieldDir//trim(this%fieldName)//'-d'//trim(procID)//'.bin', form='unformatted')

            write(1) this%nComp
            write(1) this%fieldName

            do iComp=1,this%nComp
                write(1) this%phi(:,iComp)
                write(1) iComp
                write(1) iComp
            end do

            close(1)

        end if

    end subroutine writeFieldFunction
    
end module continuityErrors