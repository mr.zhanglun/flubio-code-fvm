!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

    subroutine burgersEqn

    !==================================================================================================================
    ! Description:
    !! burgersEqn implements the advection-diffusion equation.
    !==================================================================================================================

        use transportEquation
        use fieldvar
        use globalMeshVar
        use gradient
        use physicalConstants

        implicit none

        integer :: nCorr, reusePC
    !------------------------------------------------------------------------------------------------------------------

        ! reset coeffs
        call Teqn%resetCoeffs()
        reusePC = 0

        !--------------------------!
        ! Update "Old" terms       !
        !--------------------------!

        call T%updateOldFieldValues()
        call rho%updateOldFieldValues()

        !--------------------------!
        ! Assemble diffusion term  !
        !--------------------------!

        call Teqn%diffFluxes%assembleDiffusionFluxes(T, nu)
        Teqn%diffFluxes%fluxVf = 0.0

        !--------------------------!
        ! Assemble convective term !
        !--------------------------!

        mf = faceFlux(T, Teqn%convOpt, 0.5)

        if(Teqn%convImp==0) then
            call Teqn%convFluxes%assembleConvectiveTerm(T, Teqn%convOpt)
        else
            call Teqn%convFluxes%assembleConvectiveTermImplicit(T, Teqn%convOpt)
        endif

        !--------------------------!
        ! Assemble linear system   !
        !--------------------------!

        ! Transient
        if(steadySim==0) then
            call Teqn%addTransientTerm(T, rho)
        end if

        ! Diffusion
        call Teqn%addDiffusionTerm()

        ! Convection
        if(Teqn%convImp==0) then
            call Teqn%addConvectiveTerm()
        else
            call Teqn%addConvectiveTermImplicit()
        end if

        ! Additional source term
        if(flubioOptions%volumeSources==1) call Teqn%addCustomSourceTerm(T)

        ! Boundary conditions
        call Teqn%applyBoundaryConditions(T, nu)

        ! Save the RHS with no cross diffusion term, reuse coeffs in non-orthogonal corrections
        Teqn%bCDdtQ = Teqn%bC

        !-------------------------------------!
        ! Solve convection-diffusion equation !
        !-------------------------------------!

        do nCorr=1, flubioOptions%nCorr+1

            ! Update with the new cross diffusion
            call Teqn%updateRhsWithCrossDiffusion(T, nu, T%nComp)

            ! Solve the equation
            call Teqn%solve(T, setGuess=1, reusePC=reusePC, reuseMat=nCorr)

            ! Update boundary fields
            call T%updateBoundaryField()

            ! Compute gradient
            call computeGradient(T)

            ! reset RHS
            Teqn%bC = Teqn%bCDdtQ

            ! Resuse preconditioner
            reusePC = 1

        end do

    end subroutine burgersEqn

! *********************************************************************************************************************

    subroutine checkBurgersConvergence(convergenceResidual, scalingRef, resmax)

    !==================================================================================================================
    ! Description:
    !! checkConvergence checks if the convergence criteria are satified or if the simulation has exceeded
    !! the maximum number of iterations.
    !==================================================================================================================

        use flubioDictionaries, only: flubioControls
        use globalTimeVar
        use flubioMpi

        implicit none

        real :: convergenceResidual, scalingRef, resmax
    !------------------------------------------------------------------------------------------------------------------

        !-------------------!
        ! Check Convergence !
        !-------------------!

        if(itime>=nint(tmax)) then

            flubioControls%convergence=1

            call flubioMsg('Equation has reached the maxium number of Iterations, simulation will be stopped at the end of the current iteration...')

        elseif(convergenceResidual/scalingRef <= resmax) then

            flubioControls%convergence=1

        endif

end subroutine checkBurgersConvergence

! *********************************************************************************************************************

