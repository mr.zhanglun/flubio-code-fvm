!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module WALEModel

    use LES

    implicit none

    type, public, extends(subgridModel) :: WALE

        real :: Cw

    contains
        procedure ::  modelConstants => setupWale
        procedure ::  updateEddyViscosity => waleEddyViscosity
    end type WALE

contains

    subroutine setupWale(this)

    !==================================================================================================================
    ! Description:
    !! setupWale sets up the WALE model.
    !==================================================================================================================

        class(WALE) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: optName
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        call flubioTurbModel%json%get('Cw', this%Cw, found)
        call raiseKeyErrorJSON('Cw', found)

        call this%createLesModel()

    end subroutine setupWale

! *********************************************************************************************************************

    subroutine waleEddyViscosity(this)

    !==================================================================================================================
    ! Description:
    !! subgridWALE implementes the Wall Scale Adaptive LES model.
    !==================================================================================================================

        class(WALE) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: Ssum(numberOfElements), SdSum(numberOfElements)

        real :: A, B, C, W, coeff
    !------------------------------------------------------------------------------------------------------------------

        !---------------------------------!
        ! Compute strain tensors S and Sd !
        !---------------------------------!

        Ssum = strainTensorInnerProduct()
        SdSum = strainTensorInnerProduct_d()

        !-----------------------!
        ! Compute eddyviscosity !
        !-----------------------!

        do iElement=1, numberOfElements

            A=(SdSum(iElement))**(3.0/2.0)
            B=(Ssum(iElement))**(5.0/2.0)
            C=(Sdsum(iElement))**(5.0/4.0)

            W = A/(B+C)
            coeff = rho%phi(iElement,1)*(this%Cw*this%fwidth(iElement))**2
            this%nut%phi(iElement,1) = coeff*W
            nu%phi(iElement,1) = viscos + coeff*W

        enddo

        ! Update nu and nut at boundaries
        call this%nutWallFunction()

        call this%nut%updateGhosts()
        call nu%updateGhosts()

    end subroutine waleEddyViscosity

! *********************************************************************************************************************

end module WALEModel