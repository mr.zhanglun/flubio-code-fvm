!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____ 	        !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|	        !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |     	        !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |     	        !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____ 	        !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|	        !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module kappaOmegaSST

!==================================================================================================================
! Description:
!! this module contains the implementation of the k-omega SST model.
!==================================================================================================================

    use kappaOmegaBSL

    implicit none

    type, public, extends(kwBSL) :: kwSST

        real :: a1

    contains

        procedure :: modelCoeffs => modelCoeffsSST
        procedure :: overrideDefaultCoeffs_SST
        procedure :: printCoeffs => printCoeffsKappaOmegaSST

        procedure :: updateEddyViscosity  => updateEddyViscositySST
        procedure :: updateEddyViscositySST2003

    end type kwSST

contains

!**********************************************************************************************************************

    subroutine modelCoeffsSST(this)

    !==================================================================================================================
    ! Description:
    !! modelCoeffsBSL sets the turbulence model coefficients according to NASA.
    !==================================================================================================================

        class(kwSST) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: val
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        !----------------------!
        ! Default Coefficients !
        !----------------------!

        this%Cmu = 0.09
        this%kappa = 0.41
        this%E = 9.80
        this%B = 5.25

        this%a1 = 0.31

        this%C_a1 = 0.52d0 !5.0d0/9.0d0
        this%C_a2 = 0.4404d0

        this%C_b1 = 0.072d0 !0.075d0
        this%C_b2 = 0.0828d0

        this%bstar = 0.09d0

        this%sigma_k1 = 0.85d0
        this%sigma_o1 = 0.5d0

        this%sigma_k2 = 1.0
        this%sigma_o2 = 0.856

        !----------------------------!
        ! Override from dictionaries !
        !----------------------------!

        call this%overrideDefaultCoeffs()

        this%gamma_1 = this%C_b1/this%bstar - this%sigma_o1*this%kappa**2/sqrt(this%bstar)
        this%gamma_2 = this%C_b2/this%bstar - this%sigma_o2*this%kappa**2/sqrt(this%bstar)

        call this%laminarYPlus()

        !-------------------!
        ! Print if required !
        !-------------------!

        call flubioTurbModel%json%get('PrintCoefficients', val, found)
        if(found .and. (lowercase(val)=='yes' .or. lowercase(val)=='on')) call this%printCoeffs()

    end subroutine modelCoeffsSST

! *********************************************************************************************************************

    subroutine overrideDefaultCoeffs_SST(this)

    !==================================================================================================================
    ! Description:
    !! overrideDefaultCoeffs_SST sets the coefficients as you might have defined in turbulenceModel dictionary.
    !==================================================================================================================

        class(kwSST) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: coeff
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        ! C_mu
        call flubioTurbModel%json%get('Cmu', coeff, found)
        if(found) this%Cmu = coeff

        ! kappa
        call flubioTurbModel%json%get('kappa', coeff, found)
        if(found) this%kappa = coeff

        ! E
        call flubioTurbModel%json%get('E', coeff, found)
        if(found) this%E = coeff

        ! B
        call flubioTurbModel%json%get('B', coeff, found)
        if(found) this%B = coeff

        ! a1
        call flubioTurbModel%json%get('a1', coeff, found)
        if(found) this%a1 = coeff

        ! Ca1
        call flubioTurbModel%json%get('Ca1', coeff, found)
        if(found) this%C_a1 = coeff

        ! Ca2
        call flubioTurbModel%json%get('Ca2', coeff, found)
        if(found) this%C_a2 = coeff

        ! Cb1
        call flubioTurbModel%json%get('Cb1', coeff, found)
        if(found) this%C_b1 = coeff

        ! Cb2
        call flubioTurbModel%json%get('Cb2', coeff, found)
        if(found) this%C_b2 = coeff

        ! bstar
        call flubioTurbModel%json%get('bstar', coeff, found)
        if(found) this%bstar = coeff

        ! sigma_k1
        call flubioTurbModel%json%get('sigma_k1', coeff, found)
        if(found) this%sigma_k1 = coeff

        ! sigma_o1
        call flubioTurbModel%json%get('sigma_o1', coeff, found)
        if(found) this%sigma_o1 = coeff

        ! sigma_k2
        call flubioTurbModel%json%get('sigma_k2', coeff, found)
        if(found) this%sigma_k2 = coeff

        ! sigma_o2
        call flubioTurbModel%json%get('sigma_o2', coeff, found)
        if(found) this%sigma_o2 = coeff

    end subroutine overrideDefaultCoeffs_SST

!**********************************************************************************************************************

    subroutine updateEddyViscositySST(this)

    !==================================================================================================================
    ! Description:
    !! updateEddyViscositySST update the eddy viscosity for the k-w SST model.
    !==================================================================================================================

        class(kwSST) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: Omega(numberOfElements)

        real :: density, tke, tdr, m, F1, F2, SMALL
    !------------------------------------------------------------------------------------------------------------------

        ! Prepare
        SMALL=0.0000000001d0

        Omega = rotationTensorModule()

        !-------------------------!
        ! Compute bstar*rho*omega !
        !-------------------------!

        do iElement=1,numberOfElements

            density = rho%phi(iElement,1)
            tke = max(this%tke%phi(iElement,1),0.0)
            tdr = this%tdr%phi(iElement,1)

            call this%blendingFunction(F1, F2, iElement)
            m = max(this%a1*tdr, Omega(iElement)*F2)

            this%nut%phi(iElement,1) = this%a1*density*tke/(m+SMALL)

        enddo

        ! physcisal viscosity
        nu%phi(1:numberOfElements,1) = viscos + this%nut%phi(1:numberOfElements,1)

        ! Correct eddy viscosity at wall boundary cells
        call this%nutWallFunction()

        ! Bound eddy viscosity
        call bound(this%nut, 0.0, flubioTurbModel%boundType)

        ! Update eddy and total viscosity
        call nu%updateGhosts()
        call this%nut%updateGhosts()

    end subroutine updateEddyViscositySST

!**********************************************************************************************************************

    subroutine updateEddyViscositySST2003(this)

    !==================================================================================================================
    ! Description:
    !! updateEddyViscositySST update the eddy viscosity for the SST model, 2003 version.
    !==================================================================================================================

        class(kwSST) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real ::  magS(numberOfElements)

        real :: density, tke, tdr, m, F1, F2, SMALL
    !------------------------------------------------------------------------------------------------------------------

        ! Prepare
        SMALL=0.0000000001d0

        magS = strainTensorModule()

        !-------------------------!
        ! Compute bstar*rho*omega !
        !-------------------------!

        do iElement=1,numberOfElements

            density = rho%phi(iElement,1)
            tke = max(this%tke%phi(iElement,1),0.0)
            tdr = this%tdr%phi(iElement,1)

            call this%blendingFunction(F1, F2, iElement)
            m = max(this%a1*tdr, magS(iElement)*F2)

            this%nut%phi(iElement,1) = this%a1*density*tke/(m+SMALL)

        enddo

        ! Physcisal viscosity
        nu%phi(1:numberOfElements,1) = viscos + this%nut%phi(1:numberOfElements,1)

        ! Correct eddy viscosity at wall boundary cells
        call this%nutWallFunction()

        ! Update eddy and total viscosity
        call nu%updateGhosts()
        call this%nut%updateGhosts()

    end subroutine updateEddyViscositySST2003

!**********************************************************************************************************************

    subroutine printCoeffsKappaOmegaSST(this)

    !==================================================================================================================
    ! Description:
    !! printCoeffsKappaOmegaSST prints coefficients to screen.
    !==================================================================================================================

        class(kwSST) :: this
    !------------------------------------------------------------------------------------------------------------------

        if(id==0) then
            write(*,'(A)') '!----------------------------------!'
            write(*,'(A)') '! Kappa-Omega SST coefficients:    !'
            write(*,'(A)') '!----------------------------------!'
            write(*,*)
            write(*,'(A, f6.4)') ' Cmu = ', this%Cmu
            write(*,'(A, f6.4)') ' kappa = ', this%kappa
            write(*,'(A, f6.4)') ' Ca1 = ', this%C_a1
            write(*,'(A, f6.4)') ' Ca2 = ', this%C_a2
            write(*,'(A, f6.4)') ' Cb1 = ', this%C_b1
            write(*,'(A, f6.4)') ' Cb2 = ', this%C_b2
            write(*,'(A, f6.4)') ' a1 = ', this%a1
            write(*,'(A, f6.4)') ' bstar = ', this%bstar
            write(*,'(A, f6.4)') ' sigma_k1 = ', this%sigma_k1
            write(*,'(A, f6.4)') ' sigma_k2 = ', this%sigma_k2
            write(*,'(A, f6.4)') ' sigma_o1 = ', this%sigma_o1
            write(*,'(A, f6.4)') ' sigma_o2 = ', this%sigma_o2
            write(*,'(A)') ' !---------------------------------!'
            write(*,*)
        end if

    end subroutine printCoeffsKappaOmegaSST

end module kappaOmegaSST