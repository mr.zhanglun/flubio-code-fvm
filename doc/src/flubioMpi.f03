!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module flubioMpi

!==================================================================================================================
! Description:
!! This module implements some basic mpi calls.
!==================================================================================================================

    use mpi

    implicit none

    !----------------------!
    ! MPI common variables !
    !----------------------!

    integer id
    !! processor rank number

    integer nid
    !! number of ranks employed in the simulation. This is the equivalent of "np" in mpirun/mpiexec command.

contains

    subroutine setMPIvar

    !==================================================================================================================
    ! Description:
    !! setMPIvar assign an ID number to the ranks and it counts the total number of processors (nid) involved in the computations.
    !==================================================================================================================

        ! Local variables
        integer ierr

        ! Assing an identity to processors
        call MPI_COMM_RANK(MPI_COMM_WORLD,id,ierr)
        call MPI_Comm_size(MPI_COMM_WORLD,nid,ierr)

    end subroutine setMPIvar

! *********************************************************************************************************************

    subroutine flubioStop

    !==================================================================================================================
    ! Description:
    !!flubioStop is a useful and simple subroutine for debugging. When it is called, the program aborts and saves a field file.
    !! Additionally, also momentum and pressure equation coeffs can be exported from the code for further inspections.
    !===================================================================================================================

        integer :: ierr, ierr2
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        call mpi_barrier(MPI_COMM_WORLD,ierr)
        call mpi_abort(MPI_COMM_WORLD, ierr, ierr2)

    end subroutine flubioStop

! *********************************************************************************************************************
    
    subroutine flubioStopMsg(msg)

    !==================================================================================================================
    ! Description:
    !!flubioStop is a useful and simple subroutine for debugging. When it is called, the program aborts and saves a field file.
    !! Additionally, also momentum and pressure equation coeffs can be exported from the code for further inspections.
    !==================================================================================================================

        character(*) :: msg
        !! mesage to print
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr, ierr2
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        if(id==0) write(*,*) trim(msg)
        call mpi_barrier(MPI_COMM_WORLD,ierr)
        stop
        !call mpi_abort(MPI_COMM_WORLD, ierr, ierr2)

    end subroutine flubioStopMsg

! *********************************************************************************************************************

    subroutine flubioMsg(msg)

    !==================================================================================================================
    ! Description:
    !! flubioMsg print at screen a message.
    !==================================================================================================================

        character(*) :: msg
        !! mesage to print
    !------------------------------------------------------------------------------------------------------------------

        if(id==0) then
            write(*,*)
            write(*,*) trim(msg)
            write(*,*)
        end if

    end subroutine flubioMsg

end module flubioMpi

