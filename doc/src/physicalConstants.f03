!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module physicalConstants

    use flubioFields
    use flubioTensors
    use generalDictionaries
    use meshvar

    implicit none

    type(generalDict) :: physical

    real viscos
    !! Fluid viscosity

    real dens
    !! Fluid density

    type  myConstants

        character(:), allocatable :: interpType
        !! Face interpolation type for physical quantities

        integer :: reology
        !!  Reologic model

        real :: a
        !!  Reological constant a

        real :: k
        !!  Reological constant k

        real :: m
        !!  Reological constant m

        real :: n
        !!  Reological constant n

        real :: nu0
        !!  Reological constant nu0

        real :: nuInf
        !!  Reological constant nuInf

        real :: nuMin
        !!  Reological constant nuMax

        real :: nuMax
        !!  Reological constant nuMax

        real :: tau0
        !!  Reological constant tau0

        ! Thermal properties

        real :: Pr
        !! Prandtl number

        real :: g(3)
        !! Gravity

    end type

    type(myConstants):: phys

    type(flubioField), target :: nu
    type(flubioField), target :: rho

contains

    subroutine readPhysicalConstants

    !==================================================================================================================
    ! Description:
    !! Read density, viscosity and other parameters from a file.
    !==================================================================================================================

        character(len=:), allocatable:: fluid, dictName
    !------------------------------------------------------------------------------------------------------------------

        real :: myRho
        !! density

        real :: myNu
        !! viscosity
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'physical/constants'
        call physical%initJSON(dictName)

        call physical%json%get('Fluid', fluid, found)
        call raiseKeyErrorJSON('Fluid', found)

        call physical%json%get('Density', myRho, found)
        call raiseKeyErrorJSON('Density', found)

        call physical%json%get('Viscosity', myNu, found)
        call raiseKeyErrorJSON('Viscosity', found)

        viscos = myNu
        dens = myRho

        ! Face interpolation
        call physical%json%get('FaceInterpolation', phys%interpType, found)
        if(.not. found)  phys%interpType = 'harmonic'

        !-----------------!
        ! Viscosity Model !
        !-----------------!

        if(lowercase(fluid)=='newtonian') then

            phys%reology=0

        elseif(lowercase(fluid)=='powerlaw') then

            phys%reology=1

            call physical%json%get('k', phys%k, found)
            call raiseKeyErrorJSON('k', found)

            call physical%json%get('n', phys%n, found)
            call raiseKeyErrorJSON('n', found)

            call physical%json%get('numin', phys%nuMin, found)
            call raiseKeyErrorJSON('numin', found)

            call physical%json%get('numax', phys%nuMax, found)
            call raiseKeyErrorJSON('numax', found)

        elseif(lowercase(fluid)=='birdcarreau') then

            phys%reology=2

            call physical%json%get('nu0',  phys%nu0, found)
            call raiseKeyErrorJSON('nu0', found)

            call physical%json%get('n', phys%n, found)
            call raiseKeyErrorJSON('n', found)

            call physical%json%get('a',  phys%a, found)
            call raiseKeyErrorJSON('a', found)

        elseif(lowercase(fluid)=='crosspowerlaw') then

            phys%reology=3

            call physical%json%get('nu0',  phys%nu0, found)
            call raiseKeyErrorJSON('nu0', found)

            call physical%json%get('n', phys%n, found)
            call raiseKeyErrorJSON('n', found)

            call physical%json%get('m',  phys%m, found)
            call raiseKeyErrorJSON('m', found)

        elseif(lowercase(fluid)=='herschelbulkley') then

            phys%reology=4

            call physical%json%get('nu0',  phys%nu0, found)
            call raiseKeyErrorJSON('nu0', found)

            call physical%json%get('n', phys%n, found)
            call raiseKeyErrorJSON('n', found)

            call physical%json%get('k',  phys%k, found)
            call raiseKeyErrorJSON('k', found)

            call physical%json%get('tau0',  phys%tau0, found)
            call raiseKeyErrorJSON('tau0', found)

        else

            if(id==0) write(*,*) 'FLUBIO: Unknown reological model "', fluid,'". Check the spelling!'

        endif

    end subroutine readPhysicalConstants

! *********************************************************************************************************************

    subroutine readPhysicalConstantsPoisson

    !==================================================================================================================
    ! Description:
    !! Read physical constants from a file.
    !==================================================================================================================

        character(len=:), allocatable:: dictName, material
    !------------------------------------------------------------------------------------------------------------------

        real :: rho
        !! density

        real :: kDiff
        !! diffusion coefficient (viscosity)
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'physical/constants'
        call physical%initJSON(dictName)

        call physical%json%get('Material', material, found)
        ! call raiseKeyErrorJSON('Material', found)

        call physical%json%get('rho', rho, found)
        call raiseKeyErrorJSON('rho', found)

        call physical%json%get('mu', kDiff, found)
        call raiseKeyErrorJSON('mu', found)

        viscos = kDiff
        dens = rho

        ! Face interpolation
        call physical%json%get('FaceInterpolation', phys%interpType, found)
        if(.not. found) phys%interpType = 'harmonic'

    end subroutine readPhysicalConstantsPoisson

! **********************************************************************************************************************

    subroutine readPhysicalConstantsPotential

    !==================================================================================================================
    ! Description:
    !! Read density and viscosity from a file.
    !==================================================================================================================

        character(len=:), allocatable:: dictName, fluid
    !------------------------------------------------------------------------------------------------------------------

        real :: rho
        !! density

        real :: kDiff
        !! diffusion coefficient (viscosity)
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'physical/constants'
        call physical%initJSON(dictName)

        call physical%json%get('Fluid', fluid, found)
        call raiseKeyErrorJSON('Fluid', found)

        call physical%json%get('Density', rho, found)
        call raiseKeyErrorJSON('Density', found)

        call physical%json%get('Viscosity', kDiff, found)
        call raiseKeyErrorJSON('Viscosity', found)

        viscos = kDiff
        dens = rho

        ! Face interpolation
        call physical%json%get('FaceInterpolation', phys%interpType, found)
        if(.not. found)  phys%interpType = 'harmonic'

    end subroutine readPhysicalConstantsPotential

!**********************************************************************************************************************

    subroutine updateTransportModel()

    !==================================================================================================================
    ! Description:
    !! updateTransportModel updates density and viscosity as function of cositutive law or temperature.
    !==================================================================================================================

        if(phys%reology==1) then

            call powerLaw()

        elseif(phys%reology==2) then

            call birdCarreau()

        elseif(phys%reology==3) then

            call crossPowerlaw()

        elseif(phys%reology==4) then

            call herschelBulkley()

        end if

        call rho%updateOldFieldValues()

    end subroutine updateTransportModel

!**********************************************************************************************************************

    subroutine powerLaw()

    !==================================================================================================================
    ! Description:
    !! powerLaw computes the viscosity according to the power law.
    !==================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: k, n

        real :: gamma(numberOfElements+numberOfBFaces)
    !------------------------------------------------------------------------------------------------------------------

        k = phys%k
        n = phys%n

        gamma = strainRate()
        nu%phi(:,1) = k*gamma**(n-1)

    end subroutine powerLaw

!**********************************************************************************************************************

    subroutine birdCarreau()

    !==================================================================================================================
    ! Description:
    !! birdCarreau computes the viscosity according to the Bird-Carreau formulation.
    !==================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: nu0, nuInf, k, a, n, na

        real :: gamma(numberOfElements+numberOfBFaces)
    !------------------------------------------------------------------------------------------------------------------

        a = phys%a
        k = phys%k
        n = phys%n
        nu0 = phys%nu0
        nuInf = phys%nuInf

        na = (n-1)/a

        gamma = strainRate()
        nu%phi(:,1) = nuInf + (nu0-nuInf)*(1.0+(k*gamma)**a)**na

    end subroutine birdCarreau

!**********************************************************************************************************************

    subroutine crossPowerlaw()

    !==================================================================================================================
    ! Description:
    !! crossPowerlaw computes the viscosity using the cross power law.
    !==================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: nu0, nuInf, k, n, m

        real :: gamma(numberOfElements+numberOfBFaces)
    !------------------------------------------------------------------------------------------------------------------

        m = phys%m
        k = phys%k
        n = phys%n
        nu0 = phys%nu0
        nuInf = phys%nuInf

        gamma = strainRate()
        nu%phi(:,1) = nuInf + (nu0-nuInf)/(1.0+(m*gamma)**n)

    end subroutine crossPowerlaw

!**********************************************************************************************************************

    subroutine herschelBulkley()

    !==================================================================================================================
    ! Description:
    !! crossPowerlaw computes the viscosity using the cross power law.
    !==================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: nu0, k, tau0, val, hb, n, small

        real :: gamma(numberOfElements+numberOfBFaces)
    !------------------------------------------------------------------------------------------------------------------

        small = 0.0000000001

        tau0 = phys%tau0
        k = phys%k
        n = phys%n
        nu0 = phys%nu0

        gamma = strainRate()

        do iElement=1,numberOfElements+numberOfBFaces
            hb = tau0/(gamma(iElement)+small) + k*(gamma(iElement))**(n-1)
            val = min(nu0, hb)
            nu%phi(iElement,1) = val
        end do

    end subroutine herschelBulkley

!**********************************************************************************************************************

    function strainRate() result(gamma)

    !==================================================================================================================
    ! Description:
    !! strainRate computes the strain rate used in the costitutive laws.
    !==================================================================================================================

        use fieldvar,  only : velocity
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: dudx, dudy, dudz

        real :: dvdx, dvdy, dvdz

        real :: dwdx, dwdy, dwdz

        real :: gamma(numberOfElements+numberOfBFaces)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements+numberOfBFaces

            dudx = velocity%phiGrad(iElement,1,1)
            dudy = velocity%phiGrad(iElement,1,2)
            dudz = velocity%phiGrad(iElement,1,3)

            dvdx = velocity%phiGrad(iElement,2,1)
            dvdy = velocity%phiGrad(iElement,2,2)
            dvdz = velocity%phiGrad(iElement,2,3)

            dwdx = velocity%phiGrad(iElement,3,1)
            dwdy = velocity%phiGrad(iElement,3,2)
            dwdz = velocity%phiGrad(iElement,3,3)

            gamma(iElement) = (dudx**2 + dvdy**2 + dwdz**2) + 2.0*((0.5*(dvdx+dudy))**2+(0.5*(dwdx+dudz))**2+(0.5*(dwdy+dvdz))**2)

        end do

    end function strainRate

!**********************************************************************************************************************

end module physicalConstants