!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module globalMeshVar

    implicit none

    integer :: numberOfPoints
    !! Number of points in each processor domain

    integer :: numberOfElements
    !! Number of cells in each processor domain

    integer :: numberOfIntFaces
    !! Number of internal faces in each processor domain

    integer :: numberOfFaces
    !! Number of faces in each processor domain (internal + boundary faces)

    integer :: numberOfBElements
    !! Number of boudary cells in each processor domain

    integer :: numberOfBFaces
    !! Number of boundary faces in each processor domain

    integer :: numberOfBoundaries
    !! Number of domain boundaries

    integer :: numberOfElementNodes
    !! Number vertex in each processor domain

    integer :: maxNeigh
    !! Maximum number of neighbours expected in the mesh

    integer :: numberOfProcBound
    !! Number of processor boundaries in the processor domain

    integer :: numberOfProcBound1
    !! Same as numberOfProcBound, but its value is 1, if you are running in serial

    integer :: numberOfPeriodicBound
    !!  Number of periodic boundaries found in the domain

    integer :: numberOfProcCyclic
    !! Number of periodic boundaries found in the domain which requires MPI communications (i.e. peridiodic patches located ownerd by differest ranks)

    integer :: totalPerBound
    !! Sum of numberOfPeriodicBound and numberOfProcCyclic

    integer :: bdim
    !!  Flag actived for bidimensional simulations

    integer :: numberOfWallBound
    !! Number of wall boundaries in the domain.  A "wall" boundaries is considered to be a solid wall, where velocity/stress can be applied

    integer :: maxFaces
    !! Maxium number of faces composing grid elements.  It is arbitrary set to 6, limiting the type of elements. Togheter with maxNeigh, can be changed to fit user's need.

    integer :: numberOfRealBound
    !! Number of physical boundaries (it exclude the processor boundaries)

    integer :: maxProcFaces
    !! Maxium number of shared faces among the mpi ranks

    integer, dimension(:), allocatable :: numberOfFaceNodes
    !!  Number vertices composing each face.

    integer, dimension(:), allocatable :: numberOfElementFaces
    !! Number of faces composing each cell.

    integer, dimension(:), allocatable :: cyclic
    !! Array containg the position of the cyclic patches in the boundary file.

end module globalMeshVar