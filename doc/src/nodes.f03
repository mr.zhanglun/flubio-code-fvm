!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module nodes

!==================================================================================================================
! Description:
!! This modules implements the data structures and methods for the mesh nodes.
!==================================================================================================================

    use globalMeshVar
    use flubioMpi

    implicit none

    type, public ::  grid_nodes

        integer :: numberOfPoints
        !! Number of vertex in the mesh

        real, dimension(:,:), allocatable :: vertex
        !! Cartesian coordinates of each vertex in the mesh

    contains
        procedure :: readMeshPoints
    end type

contains

    subroutine readMeshPoints(this)

    !==================================================================================================================
    ! Description:
    !! readMeshPoints reads the nodes composing the mesh from the mesh files.
    !==================================================================================================================

        class(grid_nodes) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10) :: procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
        !! loop index

        integer :: np
        !! number of mesh nodes
    !------------------------------------------------------------------------------------------------------------------

        write(procID, '(i0)' ) id

        ! Read Points
        open(1,file='grid/processor'//trim(procID)//'/FLUBIO_points')
            read(1,*) np
        close(1)

        !-------------------------!
        ! Read points coordinates !
        !-------------------------!

        allocate(this%vertex(np,3))

        ! Skip number of points
        open(1,file='grid/processor'//trim(procID)//'/FLUBIO_points')
            read(1,*)
            do n=1,np
                read(1,*) this%vertex(n,1), this%vertex(n,2), this%vertex(n,3)
            enddo
        close(1)

        this%numberOfPoints = np
        numberOfPoints = np

    end subroutine readMeshPoints

end module nodes
