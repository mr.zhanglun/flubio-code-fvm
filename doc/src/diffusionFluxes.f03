!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module diffusionFluxes

!==================================================================================================================
! Description:
!! DiffusionFluxes contains the data structure and the methods to assemble the diffusion fluxes at
!! mesh faces, arising from the numerical approximation of the diffusion term.
!! The fluxes are then used to assemble the diffusion term in the equation matrix and rhs.
!==================================================================================================================

    use flubioFields
    use meshvar
    use interpolation
    use physicalConstants

    implicit none

    type, public :: diffusive_fluxes

        real, dimension(:,:), allocatable :: fluxC1f
        !! Owner contribuiton to the convective flux

        real, dimension(:,:), allocatable :: fluxC2f
        !! Neighbour contribuiton to the convective flux

        real, dimension(:,:), allocatable :: fluxVf
        !! Explicit contribution to the convective flux (Cross diffusion + boundary conditions)

        real, dimension(:,:), allocatable :: fluxTf
        !! Total diffusive flux (total diffusion flux throught a face)

        real :: lambda
        !! Non-orthogonal correction limiter

        contains

            procedure :: create => createDiffusionFluxes
            procedure :: destroy => destroyDiffusionFluxes
            generic ::   assembleDiffusionFluxes => assembleDiffusionFluxesFieldDiffusivity, assembleDiffusionFluxesWithConstantDiffusivity
            procedure :: assembleDiffusionFluxesFieldDiffusivity
            procedure :: assembleDiffusionFluxesWithConstantDiffusivity
            procedure :: assembleDiffusionFluxesWithoutCrossDiffusion
            generic ::   updateCrossDiffusion => updateCrossDiffusionStd, updateCrossDiffusionWithConstantDiffusivity
            procedure :: updateCrossDiffusionStd
            procedure :: updateCrossDiffusionWithConstantDiffusivity
            procedure :: totalDiffusionFluxes
            procedure :: boundaryDiffusionFluxes
            procedure :: boundaryDiffusionFluxes_gDiff

    end type diffusive_fluxes

contains

    subroutine createDiffusionFluxes(this, nComp)

    !==================================================================================================================
    ! Description:
    !! CreateDiffusionFluxes is the class constructor.
    !==================================================================================================================

        class(diffusive_fluxes) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: nComp
    !------------------------------------------------------------------------------------------------------------------

        allocate(this%fluxC1f(numberOfFaces,nComp))
        allocate(this%fluxC2f(numberOfFaces,nComp))
        allocate(this%fluxVf(numberOfFaces,nComp))
        allocate(this%fluxTf(numberOfFaces,nComp))

        this%fluxC1f = 0.0
        this%fluxC2f = 0.0
        this%fluxVf = 0.0
        this%fluxTf = 0.0

        this%lambda = flubioOptions%lambda

    end subroutine createDiffusionFluxes

! *********************************************************************************************************************

    subroutine destroyDiffusionFluxes(this)

    !==================================================================================================================
    ! Description:
    !! DestroyDiffusionFluxes is the class deconstructor
    !==================================================================================================================

        class(diffusive_fluxes) :: this
    !------------------------------------------------------------------------------------------------------------------

        deallocate(this%fluxC1f)
        deallocate(this%fluxC2f)
        deallocate(this%fluxVf)
        deallocate(this%fluxTf)

    end subroutine destroyDiffusionFluxes

! *********************************************************************************************************************

    subroutine assembleDiffusionFluxesFieldDiffusivity(this, field, nu_)

    !==================================================================================================================
    ! Description:
    !! AssembleDiffusionTerm computes the diffusion fluxes to be used in linear system assembly.
    !! For each face, we can define three quantities:
    !! \begin{eqnarray}
    !!  FluxC_1_f = \mu_f\frac{|\textbf{S}|_f}{d_{CN}} \\
    !!  FluxC_2_f = -\mu_f\frac{|\textbf{S}|_f}{d_{CN}} \\
    !!  FluxV_f = \mu_f\nabla\phi_f \cdot \textbf{T}_f
    !! \begin{eqnarray}
    !==================================================================================================================

        class(diffusive_fluxes) :: this

        type(flubioField) :: field
        !! field to be used during cross diffsuion term evaluation

        type(flubioField) :: nu_
        !! diffusion field (might be constant or variable)
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iBoundary, iComp, fComp, is, ie, iProc, pNeigh, opt
    !------------------------------------------------------------------------------------------------------------------

        fComp = field%nComp

        ! Interpolate diffusivity
        call interpolate(nu_,  interpType=phys%interpType, opt= -1)

        ! Assemble fluxes
        do iComp=1,fComp
            this%fluxC1f(:, iComp) = nu_%phif(:,1)*mesh%gDiff
            this%fluxC2f(:, iComp) = -nu_%phif(:,1)*mesh%gDiff
        end do

        ! Cross diffusion
        call this%updateCrossDiffusion(field, nu_)

    end subroutine assembleDiffusionFluxesFieldDiffusivity

!**********************************************************************************************************************

    subroutine assembleDiffusionFluxesWithoutCrossDiffusion(this, field, nu_)

    !==================================================================================================================
    ! Description:
    !! assembleDiffusionFluxesWithoutCrossDiffusion computes the diffusion fluxes to be used in linear system assembly
    !! without cross-diffusion.
    !! For each face, we can define three quantities:
    !! \begin{eqnarray}
    !!  FluxC_1_f = \mu_f\frac{|\textbf{S}|_f}{d_{CN}} \\
    !!  FluxC_2_f = -\mu_f\frac{|\textbf{S}|_f}{d_{CN}} \\
    !!  FluxV_f = 0.0
    !! \begin{eqnarray}
    !==================================================================================================================

        class(diffusive_fluxes) :: this

        type(flubioField) :: field
        !! field to be used during cross diffsuion term evaluation

        type(flubioField) :: nu_
        !! diffusion field (might be constant or variable)
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iBoundary, iComp, fComp, is, ie, iProc, pNeigh, opt
    !------------------------------------------------------------------------------------------------------------------

        real :: nu_f
        !! face viscosity

        real :: dot
        !! auxilairy variable to store a scalar product
    !------------------------------------------------------------------------------------------------------------------

        fComp = field%nComp

        ! Interpolate diffusivity
        call interpolate(nu_,  interpType=phys%interpType, opt= -1)

        ! Assemble fluxes
        do iComp=1,fComp
            this%fluxC1f(:, iComp) = nu_%phif(:,1)*mesh%gDiff
            this%fluxC2f(:, iComp) = -nu_%phif(:,1)*mesh%gDiff
        end do

    end subroutine assembleDiffusionFluxesWithoutCrossDiffusion

!**********************************************************************************************************************

    subroutine updateCrossDiffusionStd(this, field, nu_)

    !==================================================================================================================
    ! Description:
    !! updateCrossDiffusion computes the cross-diffusion term arising from mesh non-orthogonality.
    !! Cross-diffsuion is computed as:
    !! \begin{equation*}
    !!    FluxV_f = \mu_f\nabla\phi_f \cdot \textbf{T}_f
    !! \end{equation*}
    !==================================================================================================================

        class(diffusive_fluxes) :: this

        type(flubioField) :: field
        !! Field targeted by the transport equation of which the gradient at mesh faces is required

        type(flubioField) :: nu_
        !! Diffusion coefficient field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iBoundary, iComp, fComp, is, ie, iProc, pNeigh, opt, fsize
    !------------------------------------------------------------------------------------------------------------------

        real :: phiGrad_f(numberOfFaces,3,field%nComp)
        !! field gradient interpolated at mesh faces

        real :: nu_f
        !! diffusion coefficient interpolated at the cell face

        real :: dot, dot2
        !! dot product

        real :: limiter
        !! non-orthogonal correction limiter
    !------------------------------------------------------------------------------------------------------------------

        fsize = field%nComp

        !-----------------------!
        ! Interpolate gradients !
        !-----------------------!

        call interpolateGradientCorrected(phiGrad_f, field, opt=1, fComp=fsize)

        !------------------------------------!
        ! Account for cell non-orthogonality !
        !------------------------------------!

        do iComp=1,fSize

            do iFace=1,numberOfFaces

                nu_f = nu_%phif(iFace,1)

                dot = dot_product(phiGrad_f(iFace,:,iComp), mesh%Tf(iFace,:))
                dot2 = dot_product(phiGrad_f(iFace,:,iComp), mesh%Sf(iFace,:))

                if(this%lambda==1.0) then
                    limiter = 1.0
                else
                    limiter = limitCrossDiffusion(dot2, dot, this%lambda)
                end if

                this%fluxVf(iFace,iComp) = -nu_f*dot*limiter

            enddo

        enddo

    end subroutine updateCrossDiffusionStd

! *********************************************************************************************************************

    subroutine assembleDiffusionFluxesWithConstantDiffusivity(this, field, nu_f)

    !==================================================================================================================
    ! Description:
    !! AssembleDiffusionTerm computes the diffusion fluxes to be used in linear system assembly.
    !! For each face, we can define three quantities:
    !! \begin{eqnarray}
    !!  FluxC_1_f = \mu_f\frac{|\textbf{S}|_f}{d_{CN}} \\
    !!  FluxC_2_f = -\mu_f\frac{|\textbf{S}|_f}{d_{CN}} \\
    !!  FluxV_f = \mu_f\nabla\phi_f \cdot \textbf{T}_f
    !! \begin{eqnarray}
    !==================================================================================================================

        class(diffusive_fluxes) :: this

        type(flubioField) :: field
        !! field to be used during cross diffsuion term evaluation
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iBoundary, iComp, fComp, is, ie, iProc, pNeigh, opt, fsize
    !------------------------------------------------------------------------------------------------------------------

        real :: nu_f
        !! constant diffusivity
    !------------------------------------------------------------------------------------------------------------------

        fComp = field%nComp

        ! Assemble fluxes
        do iComp=1,fComp
            this%fluxC1f(:, iComp) = nu_f*mesh%gDiff
            this%fluxC2f(:, iComp) = -nu_f*mesh%gDiff
        end do

        ! Cross diffusion
        call this%updateCrossDiffusionWithConstantDiffusivity(field, nu_f)

    end subroutine assembleDiffusionFluxesWithConstantDiffusivity

!**********************************************************************************************************************

    subroutine updateCrossDiffusionWithConstantDiffusivity(this, field, nu_f)

    !==================================================================================================================
    ! Description:
    !! updateCrossDiffusion computes the cross-diffusion term arising from mesh non-orthogonality.
    !! Cross-diffsuion is computed as:
    !! \begin{equation*}
    !!    FluxV_f = \mu_f\nabla\phi_f \cdot \textbf{T}_f
    !! \end{equation*}
    !==================================================================================================================

        class(diffusive_fluxes) :: this

        type(flubioField) :: field
        !! Field targeted by the transport equation of which the gradient at mesh faces is required
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iBoundary, iComp, fComp, is, ie, iProc, pNeigh, opt, fsize
    !------------------------------------------------------------------------------------------------------------------

        real :: phiGrad_f(numberOfFaces,3,field%nComp)
        !! field gradient interpolated at mesh faces

        real :: nu_f
        !! diffusion coefficient interpolated at the cell face

        real :: dot, dot2
        !! a dot prduct

        real :: limiter
        !! non-orthogonal correction limiter
    !------------------------------------------------------------------------------------------------------------------

        fsize = field%nComp

        !-----------------------!
        ! Interpolate gradients !
        !-----------------------!

        call interpolateGradientCorrected(phiGrad_f, field, opt=1, fComp=fsize)

        !------------------------------------!
        ! Account for cell non-orthogonality !
        !------------------------------------!

        do iComp=1,fSize

            do iFace=1,numberOfFaces

                dot = dot_product(phiGrad_f(iFace,:,iComp), mesh%Tf(iFace,:))
                dot2 = dot_product(phiGrad_f(iFace,:,iComp), mesh%Sf(iFace,:))

                if(this%lambda==1.0) then
                    limiter = 1.0
                else
                    limiter = limitCrossDiffusion(dot2, dot, this%lambda)
                end if

                this%fluxVf(iFace,iComp) = -nu_f*dot*limiter

            enddo

        enddo

    end subroutine updateCrossDiffusionWithConstantDiffusivity

! *********************************************************************************************************************

    subroutine totalDiffusionFluxes(this, field, nu_)

    !==================================================================================================================
    ! Description:
    !! TotalDiffusionFluxes computes the total diffusion fluxes at internal faces only.
    !! This method must be called after assembleDiffsuionFluxes, once fluxC1f and fluxC2f are known.
    !! the total flux reads:
    !! \begin{eqnarray}
    !! FluxT_f = FluxC_1_f\phi_C - FluxC_2_f\phi_N + FluxV_f
    !! \begin{eqnarray}
    !==================================================================================================================

        class(diffusive_fluxes) :: this

        type(flubioField) :: field
        !! Field targeted by the transport equation of which the gradient at mesh faces is required

        type(flubioField) :: nu_
        !! Diffusion coefficient field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iBoundary, iOwner, iNeighbour, iComp, fComp, is, ie, iProc, pNeigh, opt
    !------------------------------------------------------------------------------------------------------------------

        real :: nu_f
        !! face viscosity

        real :: dot
        !! auxilairy variable to store a scalar product

        real :: phiC(field%nComp)
        !! owner field value

        real :: phiN(field%nComp)
        !! neighbour field value
    !------------------------------------------------------------------------------------------------------------------

        !-------------------------!
        ! Assemble Interior Faces !
        !-------------------------!

        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            phiC = field%phi(iOwner,:)
            phiN = field%phi(iNeighbour,:)

            this%fluxTf(iFace,:) = this%fluxC1f(iFace,:)*phiC + this%fluxC2f(iFace,:)*phiN + this%fluxVf(iFace,:)

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie

                pNeigh = pNeigh + 1
                iOwner = mesh%owner(iBFace)

                phiC = field%phi(iOwner,:)
                phiN = field%ghosts(pNeigh,:,iBoundary)

                this%fluxTf(iBFace,:) = this%fluxC1f(iBFace,:)*phiC + this%fluxC2f(iBFace,:)*phiN + this%fluxVf(iBFace,:)

            enddo

        enddo

        !-----------------!
        ! Real boundaries !
        !-----------------!

        call this%boundaryDiffusionFluxes(field, nu_)

    end subroutine totalDiffusionFluxes

! *********************************************************************************************************************

    subroutine boundaryDiffusionFluxes(this, field, nu_)

    !==================================================================================================================
    ! Description:
    !! BoundaryDiffusionFluxes computes the total diffusion fluxes at boundary faces only.
    !==================================================================================================================

        class(diffusive_fluxes) :: this

        type(flubioField) :: field
        !! field to be used during cross diffsuion term evaluation

        type(flubioField) :: nu_
        !! diffusion coefficient
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iBoundary, iOwner, iComp, fComp, is, ie, iProc, patchFace
    !------------------------------------------------------------------------------------------------------------------

        real :: nu_f
        !! face viscosity

        real :: dot
        !! auxilairy variable to store a scalar product

        real :: phiC(field%nComp)
        !! owner field value

        real :: phib(field%nComp)
        !! neighbour field value

        real :: gDiff(field%nComp)
        !! boundary flux
    !------------------------------------------------------------------------------------------------------------------

        fComp = field%nComp

        patchFace = numberOfElements
        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                patchFace = patchFace + 1
                iOwner = mesh%owner(iBFace)

                phiC = field%phi(iOwner,:)
                phib = field%phi(patchFace,:)

                gDiff = this%fluxC1f(iBFace, :)

                do iComp = 1,fComp
                    this%fluxTf(iBFace,iComp) = gDiff(iComp)*phiC(iComp) - gDiff(iComp)*phib(iComp) + this%fluxVf(iBFace,iComp)
                enddo

            enddo

        enddo

    end subroutine boundaryDiffusionFluxes

! *********************************************************************************************************************

    subroutine boundaryDiffusionFluxes_gDiff(this, field, nu_)

    !==================================================================================================================
    ! Description:
    !! BoundaryDiffusionFluxes_gDiff computes the total diffusion fluxes at boundary faces only using gDiff as flux.
    !==================================================================================================================

        class(diffusive_fluxes) :: this

        type(flubioField) :: field
        !! field to be used during cross diffsuion term evaluation

        type(flubioField) :: nu_
        !! diffusion coefficient
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iBoundary, iOwner, iComp, fComp, is, ie, iProc, patchFace
        !! index to loop over boundaries
    !------------------------------------------------------------------------------------------------------------------

        real :: nu_f
        !! face viscosity

        real :: dot
        !! auxilairy variable to store a scalar product

        real :: phiC(field%nComp)
        !! owner field value

        real :: phib(field%nComp)
        !! neighbour field value

        real :: gDiff
        !! nu*S/dperp
    !------------------------------------------------------------------------------------------------------------------

        patchFace = numberOfElements
        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                patchFace = patchFace + 1
                iOwner = mesh%owner(iBFace)

                phiC = field%phi(iOwner,:)
                phib = field%phi(patchFace,:)

                gDiff = nu_%phif(iBFace,1)*mesh%gDiff(iBFace)

                this%fluxTf(iBFace,:) = gDiff*phiC - gDiff*phib + this%fluxVf(iBFace,:)

            enddo

        enddo

    end subroutine boundaryDiffusionFluxes_gDiff

!**********************************************************************************************************************

    function limitCrossDiffusion(dot1, dot2, lambda) result(limiter)

    !==================================================================================================================
    ! Description:
    !! limitCrossDiffusion clips the amount of non-orthogonal correction which might creates instabilities.
    !==================================================================================================================

        real :: dot1, dot2
        !!

        real :: lambda
        !! limiter in input

        real :: limiter
        !! limiter to apply

        real :: small
        !! limiter to apply
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-8

        limiter = lambda*abs(dot1)/((1-lambda)*abs(dot2)+small)

        limiter = min(limiter, 1.0)

    end function limitCrossDiffusion

! *********************************************************************************************************************

end module diffusionFluxes