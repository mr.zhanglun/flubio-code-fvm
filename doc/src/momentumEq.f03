!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module momentumEq

!==================================================================================================================
! Description:
!! This module implements the momentum equation and its data structure.
!==================================================================================================================

    use physicalConstants
    use fieldvar
    use transportEq

    implicit none

    type, public, extends(transportEqn) :: momentumEqn

        type(realCellStructMat), dimension(:), allocatable :: anbd
        !! Diagonal coefficients for the momentum equation matrix, storing only the diffusion term contribution

        real, dimension(:,:), allocatable :: aCprime
        !! Diagonal coefficients for the momentum equation matrix, storing only the diffusion term + transient/under-relaxation contribution

        real, dimension(:,:), allocatable :: bCprime
        !! Right hand side for the momentum equation matrix in PRIME correction, storing only the diffusion term and transient term contribution

        real, dimension(:,:), allocatable :: aC0
        !! old time diagonal coefficients

        real, dimension(:,:), allocatable :: aC00
        !! two time levels old diagonal coefficients

        real, dimension(:,:), allocatable :: K_1, K_2
        !! RK old porjections (for fractional step only)

        contains

            procedure :: createEq => createMomentumEq
            procedure :: destroyEq => destroyMomentumEq
            procedure :: updateDiffusionCoeffs
            procedure :: applyBoundaryConditionsMomentum
            procedure :: HbyA

    end type momentumEqn

contains

    subroutine createMomentumEq(this, eqName, nComp)

    !==================================================================================================================
    ! Description:
    !! createMomentumEq is the class constructor.
    !==================================================================================================================

        class(momentumEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(*) :: eqName
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement

        integer :: nComp
    !------------------------------------------------------------------------------------------------------------------

        this%nComp = nComp

        call this%diffFluxes%create(nComp)
        call this%convFluxes%create(nComp)
        allocate(this%aC(numberOfElements, nComp))
        allocate(this%anb(numberOfElements))
        allocate(this%bC(numberOfElements, nComp))
        allocate(this%aCPrime(numberOfElements, nComp))
        allocate(this%anbd(numberOfElements))
        allocate(this%bCPrime(numberOfElements, nComp))

        this%aC = 0.0
        this%bC = 0.0
        this%aCPrime = 0.0
        this%bCPrime = 0.0

        do iElement=1,numberOfElements
            this%anb(iElement)%csize_i = mesh%numberOfNeighbours(iElement)
            this%anb(iElement)%csize_j = this%nComp
            call this%anb(iElement)%initRealMat(mesh%numberOfNeighbours(iElement), nComp)

            this%anbd(iElement)%csize_i = mesh%numberOfNeighbours(iElement)
            this%anbd(iElement)%csize_j = this%nComp
            call this%anbd(iElement)%initRealMat(mesh%numberOfNeighbours(iElement), nComp)
        end do

        allocate(this%res(nComp))
        allocate(this%convergenceResidual(nComp))
        allocate(this%scalingRef(nComp))
        allocate(this%unscaledRes(nComp))
        allocate(this%resmax(nComp))

        this%res = 0.0
        this%scalingRef = 0.0
        this%unscaledRes = 0.0
        this%convergenceResidual = 0.0
        this%resmax = 0.0

        this%eqName = eqName
        call this%allocateMatrices()

    end subroutine createMomentumEq

! *********************************************************************************************************************

    subroutine destroyMomentumEq(this)

    !==================================================================================================================
    ! Description:
    !! destroyMomentumEq is the class de-contructor.
    !==================================================================================================================

        class(momentumEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
    !------------------------------------------------------------------------------------------------------------------

        call this%diffFluxes%destroy()
        call this%convFluxes%destroy()

        deallocate(this%aC)
        deallocate(this%anb)
        deallocate(this%bC)
        deallocate(this%anbd)
        deallocate(this%aCPrime)
        deallocate(this%bCPrime)

        deallocate(this%res)
        deallocate(this%convergenceResidual)
        deallocate(this%scalingRef)
        deallocate(this%unscaledRes)
        deallocate(this%resmax)

        call MatDestroy(this%A, ierr)
        call VecDestroy(this%rhs, ierr)

    end subroutine destroyMomentumEq

! *********************************************************************************************************************

    subroutine updateDiffusionCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! updateDiffusionCoeffs stores the coefficients and rhs in their current status. The reason why this subroutine is called
    !! updateDiffusionCoeffs is that it is called bafter the computation of the diffusion term coefficients that will be reused
    !! in the correcto step of the PISO algorithm.
    !==================================================================================================================

        class(momentumEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
    !------------------------------------------------------------------------------------------------------------------

        this%aCPrime = this%aC
        call copyNonUniformMatrix(this%anbd, this%anb, numberOfElements)
        this%bCPrime = this%bC

    end subroutine updateDiffusionCoeffs

! *********************************************************************************************************************

    subroutine applyBoundaryConditionsMomentum(this)

    !==================================================================================================================
    ! Description:
    !! applyBoundaryConditionsMomentum applies the boundary conditions to the momentum equation. According to the boundary flag
    !! assigned at each boundary, this subroutine will invoke the proper subrotuine to compute the diagonal and rhs corrections.
    !==================================================================================================================

        class(momentumEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iB, iBoundary, iOwner, iBFace, iComp, is ,ie, patchFace, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: mu, aCb(3), bCb(3), phi0(3), SMALL

        real, dimension(:), allocatable :: slipLength

        real, dimension(:), allocatable :: phiInf
    !------------------------------------------------------------------------------------------------------------------

        patchFace = numberOfElements

        SMALL = 0.000000000001d0

        !--------------------------------!
        ! Interpolate physical constants !
        !--------------------------------!

        call interpolate(rho, phys%interpType, -1)
        call interpolate(nu, phys%interpType, -1)

        !---------------------------------!
        ! Loop over physical boundaries   !
        !---------------------------------!

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            bFlag = velocity%bcFlag(iBoundary)

            ! Loop over the faces of boundary number "iBoundary"
            do iBFace=is, ie

                patchFace = patchFace+1
                iOwner = mesh%owner(iBFace)
                mu = nu%phif(iBFace,1)

                ! void BC
                if(bFlag==0) then

                    call voidBoundary(aCb, bCb, 3)

                ! Wall BC
                elseif(bFlag==1) then

                    call wallBoundary(iBFace, aCb, bCb, velocity%phi(patchFace,:), mu)

                ! Slip BC
                elseif(bFlag==2) then

                    call slipBoundary(aCb, bCb)

                ! Symmetry BC
                elseif(bFlag==3) then

                    call symmetryBoundary(velocity%phi, iBFace, aCb, bCb, mu, 3)

                ! Inlet BC
                elseif(bFlag==4) then

                    call velocityInletBoundary(iBFace, aCb, bCb, velocity%phi(patchFace,:), mu, 1.0)

                ! Outlet BC
                elseif(bFlag==5) then

                    call neumann0Boundary(iBFace, aCb, bCb, 1.0, 3)

                ! Fixed Gradient BC
                elseif(bFlag==6) then

                    call neumannBoundary(iBFace, aCb, bCb, velocity%boundaryValue(iBoundary,:), mu, 1.0, 3)

                ! User defined inlet BC
                elseif(bFlag==7) then

                    call velocityInletBoundary(iBFace, aCb, bCb, velocity%phi(patchFace,:), mu, 1.0)

                ! Missing
                elseif(bFlag==8) then

                ! Dirichlet BC
                elseif(bFlag==9) then

                    call dirichletBoundary(iBFace, aCb, bCb, velocity%phi(patchFace,:), mu, 1.0, 3)

                ! Dirichlet BC
                elseif(bFlag==1000) then

                    call dirichletBoundary(iBFace, aCb, bCb, velocity%phi(patchFace,:), mu, 1.0, 3)

                ! Dirichlet BC
                elseif(bFlag==1001) then

                    call velocityInletBoundary(iBFace, aCb, bCb, velocity%phi(patchFace,:), mu, 1.0)

                ! Robin BC
                elseif(bFlag==10) then

                    call velocity%bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%slipLength', slipLength)
                    call velocity%bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%phiInf', phiInf)
                    call robinBoundary(iBoundary, iBFace, aCb, bCb, velocity%phiGrad(patchFace,:,:), velocity%phi(iBFace,:), mu, slipLength, phiInf, this%nComp)

                ! FarField
                elseif(bFlag==11) then

                    phi0 = velocity%boundaryValue(iBoundary,:)
                    call farFieldBoundary(iBFace, aCb, bCb, phi0, mu, 1.0, 3)

                else

                    if(id==0) write(*,*) 'FLUBIO: Boundary Condition not found at Boundary ', trim(mesh%boundaries%userName(iBoundary))
                    call flubioStop()

                endif

                !-------------------------------!
                ! Correct boundary coefficients !
                !-------------------------------!

                this%aC(iOwner,:) = this%aC(iOwner,:) + aCb
                this%bC(iOwner,:) = this%bC(iOwner,:) + bCb

            enddo

        enddo
        
    end subroutine applyBoundaryConditionsMomentum

! *********************************************************************************************************************

    function HbyA(this, field) result(phi)

    !==================================================================================================================
    ! Description:
    !! HbyA returns the H/aC operator, commonly in SIMPLE algorithm description, i.e. :
    !! \begin{equation}
    !!    H = [(b_C + \nabla P * vol) -\sum a_{nb}*\phi_{anb}/a_C]/a_C
    !!  \end{equation}
    !==================================================================================================================

#include "petsc/finclude/petscksp.h"
        use petscksp

        class(momentumEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        PetscReal auxD, auxNb(maxNeigh)

        Vec y
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iComp, fComp, ierr, nn

        integer :: iG, jG(maxNeigh), ii(numberOfElements), conn(numberOfElements, maxNeigh)
    !------------------------------------------------------------------------------------------------------------------

        real :: phi(numberOfElements, this%nComp), sumAnb(numberOfElements,1)

        real :: val, gradP
    !------------------------------------------------------------------------------------------------------------------

        phi = 0.0

        fComp = this%nComp - bdim

        do iComp=1,fComp

            !---------------------!
            ! Assemble the matrix !
            !---------------------!

            call MatZeroEntries(this%A, ierr)
            call VecZeroEntries(this%rhs, ierr)
            sumAnb = 0.0

            do iElement=1,numberOfelements

                iG = mesh%cellGlobalAddr(iElement)-1 ! PETSC counts from 0

                ! Diagonal value
                auxD = this%aC(iElement,iComp)
                call MatSetValue(this%A,iG,iG,auxD,INSERT_VALUES,ierr)

                ! Neighbours values
                nn = mesh%numberOfNeighbours(iElement)
                auxNb(1:nn) = this%anb(iElement)%coeffs(1:nn,iComp)

                jG(1:nn) = mesh%conn(iElement)%col(1:nn)-1
                call MatSetValues(this%A,1,iG,nn,jG(1:nn),auxNb,INSERT_VALUES,ierr)

            enddo

            call MatAssemblyBegin(this%A,MAT_FINAL_ASSEMBLY,ierr)
            call MatAssemblyEnd(this%A,MAT_FINAL_ASSEMBLY,ierr)

            !-------------------------------------!
            ! Fill a vector with the equation rhs !
            !-------------------------------------!

            ! duplicate the vector
            call VecDuplicate(this%rhs,y,ierr)

            do i=1,numberOfElements
                ii(i)=mesh%cellGlobalAddr(i)-1 ! PETSC counts from 0
            enddo

            call VecSetValues(this%rhs,numberOfElements,ii,field%phi(1:numberOfElements,iComp),INSERT_VALUES,ierr)

            call VecAssemblyBegin(this%rhs, ierr)
            call VecAssemblyEnd(this%rhs, ierr)

            !------------------------------------------------------------------------!
            ! Matrix multiplication, remember to subtract aC(iElement)*phi(iElement) !
            !------------------------------------------------------------------------!

            call MatMult(this%A, this%rhs, y, ierr)
            call VecGetValues(y, flubioSolvers%lm, ii, sumAnb(:,1), ierr)

            ! Subtract off the diagonal term and add the pressure gradient again
            do iElement=1,numberOfElements

                gradP = pressure%phiGrad(iElement,iComp,1)*mesh%volume(iElement)

                val = (this%bC(iElement,iComp) + gradP) -(sumAnb(iElement,1) &
                        -this%aC(iElement,iComp)*field%phi(iElement,iComp))

                phi(iElement,iComp) = val/this%aC(iElement, iComp)

            enddo

            ! Destroy PETSc object
            call VecDestroy(y, ierr)

        enddo

    end function HbyA

end module momentumEq