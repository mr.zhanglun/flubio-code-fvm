!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module lsolvers

!==================================================================================================================
! Description:
!! This module implements the dictionary storing the entries of settings/lsolver and associated flags.
!==================================================================================================================

    use generalDictionaries
    use globalMeshVar

        implicit none

    type, extends(generalDict) :: lsolversDict

        integer :: M
        !! Global linear system matrices size (total number of cells x total number of cells)

        integer :: lm
        !! Local linear system matrices size (NumberOfElements x numberOfElements)

        integer :: d_nz
        !! Expected number of non-zero entries in matrix diagonal block (see PETSc manual)

        integer :: o_nz
        !!  Expected number of non-zero entries in matrix oof-diagonal block (see PETSc manual)

        integer :: itmax
        !! Maxium number of linear solver iterations

        integer :: flag_pcp
        !! Flag for the pressure preconditioner

        integer :: resetPc
        !! Flag to reset the pressure preconditioner

        character(len=:), allocatable :: matOrderingType
        !! Name of the choosen linear solver for the pressure-correction equation

        character(len=:), allocatable :: scalingType
        !! Name of the choosen linear solver for the pressure-correction equation

    contains
        procedure :: set => setGlobalSolverParameters
        procedure :: getSparsity
        procedure :: setPreconditionerOption

    end type lsolversDict
    
contains

! *********************************************************************************************************************

    subroutine setGlobalSolverParameters(this)

    !==================================================================================================================
    ! Description:
    !! setGlobalSolverParameters sets some basic solver mandatory options.
    !==================================================================================================================

        class(lsolversDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! dictionary name

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: opt
        !! option value

    !------------------------------------------------------------------------------------------------------------------

        integer :: error_cnt

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'settings/linearSolvers'

        ! Initialise the JSON dictionary
        call this%initJSON(dictName)

        call this%json%get('RenumberMesh', opt, found)
        if(.not. found) opt='none'
        call bandwidthReductionSettings(this, opt)

        call this%json%get('MaxIter', this%itmax, found)
        call raiseKeyErrorJSON('MaxIter', found)

        call this%json%get('RecomputePCEvery', this%resetPc, found)
        call raiseKeyErrorJSON('RecomputePCEvery', found)

        call this%json%get('DisplayScaledResidual', this%scalingType, found)
        if(.not. found) this%scalingType = 'no'

        call this%getSparsity()

    end subroutine setGlobalSolverParameters

! *********************************************************************************************************************

    subroutine bandWidthReductionSettings(this, bandOpt)

    !==================================================================================================================
    ! Description:
    !! bandwidthReductionSettings sets the badwidth reduction method.
    !==================================================================================================================

        type(lsolversDict) :: this
    !------------------------------------------------------------------------------------------------------------------
        
        character(len=:), allocatable :: bandOpt
    !------------------------------------------------------------------------------------------------------------------

        bandOpt = lowercase(bandOpt)

        if(bandOpt=='natural') then
            this%matOrderingType='natural'
        elseif(bandOpt=='nesteddissection') then
            this%matOrderingType='nd'
        elseif(bandOpt=='onewaydissection') then
            this%matOrderingType='1wd'
        elseif(bandOpt=='reversedcuthillmckee') then
            this%matOrderingType='rcm'
        elseif(bandOpt=='quotientminimumdegree') then
            this%matOrderingType='qmd'
        elseif(bandOpt=='rowlenght') then
            this%matOrderingType='rowlenght'
        elseif(bandOpt=='wbm') then
            this%matOrderingType='wbm'
        elseif(bandOpt=='spectral') then
            this%matOrderingType='spectral'
        elseif(bandOpt=='amd') then
            this%matOrderingType='amd'
        else
            if(id==0) then
                write(*,*) 'Warning: readSettings.F95::bandwidthReductionSettings - Unknown badwidth reduction method: ', trim(bandOpt)
                write(*,*) 'Using default one: reversedCuthillMckee'
                this%matOrderingType='rcm'
            endif
        endif
        
    end subroutine bandWidthReductionSettings

! *********************************************************************************************************************

    subroutine setPreconditionerOption(this, pcOpt, pcFlag)

    !==================================================================================================================
    ! Description:
    !! setPreconditionerOption sets the preconditioner option.
    !==================================================================================================================

        class(lsolversDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: pcFlag
    !------------------------------------------------------------------------------------------------------------------

        character(*) :: pcOpt
    !------------------------------------------------------------------------------------------------------------------

        if(pcOpt=='bjacobi') then
            pcFlag=0
        elseif(pcOpt=='asm') then
            pcFlag=1
        elseif(pcOpt=='gamg') then
            pcFlag=2
        elseif(pcOpt=='hypre') then
            pcFlag=3
        elseif(pcOpt=='pjacobi') then
            pcFlag=4
        elseif(pcOpt=='sor') then
            pcFlag=5
        elseif(pcOpt=='spai') then
            pcFlag=6
        elseif(pcOpt=='kaczmarz') then
            pcFlag=7
        elseif(pcOpt=='ml') then
            pcFlag=8
        elseif(pcOpt=='none') then
            pcFlag=-1
        else
            if(id==0) write(*,*) 'ERROR: settings.F95::FLUBIO_Settings - Unknown precondtioner ', trim(pcOpt)
            call flubioStop
        endif

    end subroutine setPreconditionerOption

! *********************************************************************************************************************

    subroutine getSparsity(this)

    !==================================================================================================================
    ! Description:
    !! getSparsity sets the number of diagona and off-diagonal non zero entries in a equation matrix.
    !==================================================================================================================

        class(lsolversDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: d_nz, o_nz
    !------------------------------------------------------------------------------------------------------------------

        this%lm = numberOfElements

        this%d_nz = 0
        this%o_nz = 0

        ! Approximate number of non-zero entries
        this%d_nz = 7
        this%o_nz = 7

    end subroutine getSparsity

! *********************************************************************************************************************

end module lsolvers