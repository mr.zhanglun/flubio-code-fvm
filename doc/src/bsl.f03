!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____ 	        !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|	        !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |     	        !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |     	        !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____ 	        !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|	        !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module kappaOmegaBSL

!==================================================================================================================
! Description:
!! this module contains the implementation of the k-omega BSL model.
!==================================================================================================================

    use kappaOmegaWilcox

    implicit none

    type, public, extends(kw1988) :: kwBSL

        real :: gamma_1
        real :: gamma_2

    contains

        procedure :: modelCoeffs => modelCoeffsBSL

        procedure :: tkeDiffusivity => kappaDiffusivityBSL
        procedure :: tdrDiffusivity => omegaDiffusivityBSL

        procedure :: tdrSourceTerm => omegaSourceTermBSL
        procedure :: add_gradK_gradW

    end type kwBSL

contains

!**********************************************************************************************************************

    subroutine modelCoeffsBSL(this)

    !==================================================================================================================
    ! Description:
    !! modelCoeffsBSL sets the turbulence model coefficients according to NASA.
    !==================================================================================================================

        class(kwBSL) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: val
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        !----------------------!
        ! Default Coefficients !
        !----------------------!

        this%Cmu = 0.09
        this%kappa = 0.41

        this%C_a1 = 0.52d0 !5.0d0/9.0d0
        this%C_a2 = 0.4404d0

        this%C_b1 = 0.072d0 !0.075d0
        this%C_b2 = 0.0828d0

        this%bstar = 0.09d0

        this%sigma_k1 = 0.5d0
        this%sigma_o1 = 0.5d0

        this%sigma_k2 = 1.0
        this%sigma_o2 = 0.856

        !----------------------------!
        ! Override from dictionaries !
        !----------------------------!

        call this%overrideDefaultCoeffs()

        this%gamma_1 = this%C_b1/this%bstar - this%sigma_o1*this%kappa**2/sqrt(this%bstar)
        this%gamma_2 = this%C_b2/this%bstar - this%sigma_o2*this%kappa**2/sqrt(this%bstar)

        !-------------------!
        ! Print if required !
        !-------------------!

        call flubioTurbModel%json%get('PrintCoefficients', val, found)
        if(found .and. (lowercase(val)=='yes' .or. lowercase(val)=='on')) call this%printCoeffs()

    end subroutine modelCoeffsBSL

!**********************************************************************************************************************

    subroutine kappaDiffusivityBSL(this)

    !==================================================================================================================
    ! Description:
    !! kappaDiffusivityBSL computes the diffusion coefficient for the tke equation in the BSL model.
    !==================================================================================================================

        class(kwBSL) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iOwner, patchFace, iBFace, is, ie, iBoundary
    !------------------------------------------------------------------------------------------------------------------

        real :: sigma, F1, F2
    !------------------------------------------------------------------------------------------------------------------

        !---------------------------------!
        ! Compute effective diffusivities !
        !---------------------------------!

        do iElement=1,numberOfElements

            call this%blendingFunction(F1, F2, iElement)
            sigma = F1*this%sigma_k1 + (1-F1)*this%sigma_k2
            this%muEff_k%phi(iElement,1) = viscos + this%nut%phi(iElement,1)*sigma

        enddo

        ! Boundary Faces
        patchFace = numberOfElements
        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                patchFace=patchFace+1
                iOwner = mesh%owner(iBFace)

                call this%blendingFunction(F1, F2, iOwner)
                sigma = F1*this%sigma_k1 + (1-F1)*this%sigma_k2

                ! Do not blend at boundaries
                ! sigma = this%sigma_k1

                this%muEff_k%phi(patchFace,1) = viscos + this%nut%phi(patchFace,1)*sigma

            enddo

        enddo

        ! Update ghosts
        call this%muEff_k%updateGhosts()


    end subroutine kappaDiffusivityBSL

!**********************************************************************************************************************

    subroutine omegaDiffusivityBSL(this)

    !==================================================================================================================
    ! Description:
    !! kappaDiffusivityBSL computes the diffusion coefficient for the tdr equation in the BSL model.
    !==================================================================================================================

        class(kwBSL) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iOwner, patchFace, iBFace, is, ie, iBoundary
    !------------------------------------------------------------------------------------------------------------------

        real :: sigma, F1, F2
    !------------------------------------------------------------------------------------------------------------------

        !---------------------------------!
        ! Compute effective diffusivities !
        !---------------------------------!

        do iElement=1,numberOfElements ! + numberOfBFaces

            call this%blendingFunction(F1, F2, iElement)
            sigma = F1*this%sigma_o1 + (1.0-F1)*this%sigma_o2

            this%muEff_w%phi(iElement,1) = viscos + this%nut%phi(iElement,1)*sigma

        enddo

        ! Boundary Faces
        patchFace = numberOfElements
        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                patchFace=patchFace+1
                iOwner = mesh%owner(iBFace)

                call this%blendingFunction(F1, F2, iOwner)
                sigma = F1*this%sigma_o1 + (1-F1)*this%sigma_o2

                ! Do not blend at boundaries
                ! sigma = this%sigma_o1
                this%muEff_w%phi(patchFace,1) = viscos + this%nut%phi(patchFace,1)*sigma

            enddo

        enddo

        ! Update ghosts
        call this%muEff_w%updateGhosts()

    end subroutine omegaDiffusivityBSL

!**********************************************************************************************************************

    subroutine omegaSourceTermBSL(this)

    !==================================================================================================================
    ! Description:
    !! omegaSourceTermBSL computes source term for the tdr equation for the BSL model.
    !==================================================================================================================

        class(kwBSL) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: density, tdr, nut, Pk, vol, gamm, beta, F1, F2, gradKgradW, SMALL

        real :: alphaTilde, betaTilde

        real :: aC, bC, corrbC, corraC
    !------------------------------------------------------------------------------------------------------------------

        SMALL=0.0000000001d0

        !-------------------------!
        ! Compute bstar*rho*omega !
        !-------------------------!

        do iElement=1,numberOfElements

            tdr = this%tdr%phi(iElement,1) + SMALL
            density = rho%phi(iElement,1)
            nut = this%nut%phi(iElement,1)
            Pk = this%Pk(iElement)
            vol = mesh%volume(iElement)

            !--------------!
            ! Blend coeffs !
            !--------------!

            call this%blendingFunction(F1, F2, iElement)
            alphaTilde = F1*this%C_a1 + (1.0-F1)*this%C_a2
            betaTilde = F1*this%C_b1 + (1.0-F1)*this%C_b2

            !------------------!
            ! Cross production !
            !------------------!

            gradKgradW = this%add_gradK_gradW(iElement)
            call doubleBar(-gradKgradW/tdr, 0.0, corraC)
            call doubleBar(gradKgradW, 0.0, corrbC)

            !-------------!
            ! Source term !
            !-------------!

            aC = betaTilde*density*tdr*vol + 2*(1-F1)*this%sigma_o2*corraC*vol
            bC = (alphaTilde*Pk/(nut+small))*vol + 2*(1.0-F1)*this%sigma_o2*corrbC*vol

            this%tdrEqn%aC(iElement,1) = this%tdrEqn%aC(iElement,1) + aC
            this%tdrEqn%bC(iElement,1) = this%tdrEqn%bC(iElement,1) + bC

        enddo

        if(flubioTurbModel%sustTerm) call this%tdrSustTerm()

    end subroutine omegaSourceTermBSL

!**********************************************************************************************************************

    function add_gradK_gradW(this, iElement) result(bC)

    !==================================================================================================================
    ! Description:
    !! add_gradK_gradW computes the scalar product between the gradient of tke and the gradient of tdr.
    !==================================================================================================================

        class(kwBSL) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: density, tdr, vol

        real :: bC, gradK(3), gradW(3), dot, small
    !------------------------------------------------------------------------------------------------------------------

        small = 0.0000000001

        gradK = this%tke%phiGrad(iElement,:,1)
        gradW = this%tdr%phiGrad(iElement,:,1)
        dot = dot_product(gradK,gradW)

        density = rho%phi(iElement,1)
        tdr = this%tdr%phi(iElement,1) + small
        vol = mesh%volume(iElement)

        bC = 2.0*density*this%sigma_o2*dot*vol/tdr

    end function add_gradK_gradW

!**********************************************************************************************************************

end module kappaOmegaBSL