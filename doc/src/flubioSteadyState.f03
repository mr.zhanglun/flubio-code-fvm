!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module steadyStateControls

!===================================================================================================================
! Description:
!! This module implements the dictionary storing the entries of settings/steadyState and associated flags.
!===================================================================================================================

    use generalDictionaries
    use globalTimeVar

    implicit none

    type, extends(generalDict) :: steadyStateDict

        character(len=:), allocatable :: SIMPLEC

        real :: massResidual

    contains
        procedure :: set => setSteadyStateControls
        procedure :: setForPoisson

    end type steadyStateDict

contains

    subroutine setSteadyStateControls(this)

    !==================================================================================================================
    ! Description:
    !! setSteadyStateControls sets the control parameters for steady state simulations.
    !==================================================================================================================

        class(steadyStateDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! dictionary name

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: val
        !! value

    !------------------------------------------------------------------------------------------------------------------

        integer :: error_cnt

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'settings/steadyStateControls'

        ! Initialise the JSON dictionary
        call this%initJSON(dictName)

        ! Check if the keyword exists and set its value
        call this%json%get('SIMPLEC', this%SIMPLEC, found)
        if(.not. found)  this%SIMPLEC = 'no'

        call this%json%get('massResidual', this%massResidual, found)
        call raiseKeyErrorJSON('massResidual', found)

    end subroutine setSteadyStateControls

! *********************************************************************************************************************

    subroutine setForPoisson(this)

    !==================================================================================================================
    ! Description:
    !! setForPoisson sets the control parameters for steady state simulations using advection-diffusion equation.
    !==================================================================================================================

        class(steadyStateDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! dictionary name

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: val
        !! value
    !------------------------------------------------------------------------------------------------------------------

        integer :: error_cnt
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'settings/steadyStateControls'

        ! Initialise the JSON dictionary
        call this%initJSON(dictName)

    end subroutine setForPoisson

end module  steadyStateControls