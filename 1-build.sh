# usage:
# ./1-build.sh -all       (configures and compiles cmake build)
# ./1-build.sh -a         (configures and compiles cmake build)
# ./1-build.sh -configure (configures cmake build)
# ./1-build.sh -c         (configures cmake build)
# ./1-build.sh            (only compiles cmake build)

#-----------------------
configure() {
#-----------------------
  rm -rf 0--build && cmake -S . -B 0--build
} 

#-----------------------
build() {
#-----------------------
  cmake --build   0--build --parallel $njobs
  cmake --install 0--build
} 

#-----------------------
# main logic
#-----------------------

# define env vars: home_dir_flubio and home_dir_thirdparty
source ./flubio_env.sh

if [ $# -ge 1 ]; then
  if [ $1 == "--configure" ] || [ $1 == "-c" ]; then
    configure
  elif [ $1 == "--all" ] || [ $1 == "-a" ]; then
    configure
    build
  else
    echo unsupported option: $1
  fi
else
  build
fi

