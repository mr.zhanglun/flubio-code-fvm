
# build-parmetis

tar -xzf tarballs/parmetis-4.0.3.tar.gz

mv parmetis-4.0.3 0-parmetis

cp bash/CMakeLists.txt-parmetis 0-parmetis/CMakeLists.txt

cp bash/CMakeLists.txt-metis 0-parmetis/metis/CMakeLists.txt

cd 0-parmetis

rm -rf 0--build && mkdir 0--build 

cd 0--build

cmake  .. \
 -DCMAKE_INSTALL_PREFIX=${thirdparty_install_dir}/parmetis \
 -DCMAKE_BUILD_TYPE=Release \
 -DCMAKE_C_FLAGS="-fPIC"

make -j${njobs}
make install

# build-metis

cd ../metis/build/

cmake  .. \
 -DCMAKE_INSTALL_PREFIX=${thirdparty_install_dir}/metis

make -j${njobs}
make install

#cp build/Linux-x86_64/libmetis/       $thirdparty_install_dir/metis/lib
#cp GKlib/libGKlib.a    $thirdparty_install_dir/metis/lib
