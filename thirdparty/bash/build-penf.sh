
unzip ./tarballs/PENF.zip 

mv PENF 0-penf

cd 0-penf

rm -rf 0--build && mkdir 0--build && cd 0--build

cmake .. \
 -DCMAKE_INSTALL_PREFIX=${thirdparty_install_dir}/penf \
 -DCMAKE_BUILD_TYPE=Release \

make install

mv ${thirdparty_install_dir}/penf/lib64 ${thirdparty_install_dir}/penf/lib 
