
# build-vtkfortran

unzip tarballs/VTKFortran-master.zip

mv VTKFortran-master 0-vtk

cd 0-vtk/src/third_party/PENF/
make clean
make -j${njobs}

cd ../../../
make clean
make -j${njobs}

mkdir -p                  $thirdparty_install_dir/vtk/lib
mv static/libvtkfortran.a $thirdparty_install_dir/vtk/lib
mv static/mod/*           $thirdparty_install_dir/vtk/lib
