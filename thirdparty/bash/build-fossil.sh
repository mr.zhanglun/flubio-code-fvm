
# build-fossil

unzip ./tarballs/FOSSIL.zip 

mv FOSSIL 0-fossil

#cp bash/makefile-fossil 0-fossil/makefile
#(cd 0-fossil/ && make clean && VERBOSE=1 make )

cp bash/CMakeLists.txt-fossil 0-fossil/CMakeLists.txt

cd 0-fossil

rm -rf 0--build && mkdir 0--build 

cd 0--build

cmake  .. \
 -DCMAKE_INSTALL_PREFIX=${thirdparty_install_dir} \
 -DCMAKE_BUILD_TYPE=Release \

make 

mkdir -p        $thirdparty_install_dir/fossil/lib
cp libfossil.a  $thirdparty_install_dir/fossil/lib
mkdir -p        $thirdparty_install_dir/fossil/mod
cp mod/*        $thirdparty_install_dir/fossil/mod
