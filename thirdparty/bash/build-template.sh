
# build-X

unzip ./tarballs/X.zip 

mv X 0-X

cp bash/CMakeLists.txt-X 0-X/CMakeLists.txt

cd 0-X

rm -rf 0--build && mkdir 0--build && cd 0--build

cmake  .. \
 -DCMAKE_INSTALL_PREFIX=${thirdparty_install_dir} \
 -DCMAKE_BUILD_TYPE=Release \

make -j${njobs}
make install
