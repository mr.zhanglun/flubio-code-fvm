# CMake project file for FLUBIO

cmake_minimum_required( VERSION 3.15 )

project( FLUBIO )

set( cmake_local  ./cmake )

include( ${cmake_local}/functions/debug.cmake )

include( ${cmake_local}/configure/1-flubio.cmake )
include( ${cmake_local}/configure/2-directories.cmake )
include( ${cmake_local}/configure/3-compilers.cmake )
include( ${cmake_local}/configure/4-thirdparty.cmake )

#debug_DUMP( CMAKE_BINARY_DIR )
#debug_DUMP( CMAKE_MODULE_PATH )
#debug_DUMP( CMAKE_Fortran_FLAGS )

include( ${cmake_local}/flubio_libs_build.cmake )

include( ${cmake_local}/build/app_0_prep.cmake )

# Solvers
include( ${cmake_local}/build/app_poisson.cmake )
include( ${cmake_local}/build/app_potential.cmake )
include( ${cmake_local}/build/app_burgers.cmake )
include( ${cmake_local}/build/app_transport.cmake )
include( ${cmake_local}/build/app_simple.cmake )
include( ${cmake_local}/build/app_simplem.cmake )
include( ${cmake_local}/build/app_piso.cmake )
include( ${cmake_local}/build/app_fs.cmake )
include( ${cmake_local}/build/app_fs_ibm.cmake )

# Utilities
include( ${cmake_local}/build/app_meshview.cmake )
include( ${cmake_local}/build/app_boundary_meshview.cmake )
include( ${cmake_local}/build/app_metis_mesh.cmake )
include( ${cmake_local}/build/app_decompose_mesh.cmake )
include( ${cmake_local}/build/app_fields2p.cmake )
include( ${cmake_local}/build/app_fieldp2s.cmake )
include( ${cmake_local}/build/app_fields.cmake )
include( ${cmake_local}/build/app_boundary_fields.cmake )

include( ${cmake_local}/build/app_meshview_parallel.cmake )
include( ${cmake_local}/build/app_boundary_meshview_parallel.cmake )
include( ${cmake_local}/build/app_fields_parallel.cmake )
include( ${cmake_local}/build/app_boundary_fields_parallel.cmake )
