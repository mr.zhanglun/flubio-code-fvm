
# build-renumber

scripts_dir=$home_dir_flubio/scripts

cp $scripts_dir/bash/CMakeLists-parallelMeshConversion.txt $scripts_dir/parallelMeshConversion/CMakeLists.txt

cd $scripts_dir/parallelMeshConversion

rm -rf 0--build && mkdir 0--build 

cd 0--build

cmake  .. \
   -DCMAKE_RUNTIME_OUTPUT_DIRECTORY=$scripts_dir/0---install/bin

make -j$njobs
