	program renumberMesh

	implicit none

	character(len=10) procID, proc

	character(len=10), dimension(:), allocatable :: args

	character(len=6) :: fmt
!------------------------------------------------------------------------------------------------------------------

	integer :: i, j, k, p, iElement, numberOfProc, numberOfElements, anumber

	integer :: is, ie

	integer, dimension(:), allocatable:: dummyVec

	integer :: fnumber, np, nargs, stat
!------------------------------------------------------------------------------------------------------------------

		!------------------------!
		! command line arugments !
		!------------------------!

		nargs = command_argument_count()
		
		if(nargs>0) then

			allocate(args(nargs))

			do p=1,nargs
				call get_command_argument(number=p, value=args(p), status=stat)
			enddo

			! Process the options
			do p=1,nargs
				if(trim(args(p))=='-np' .or. args(p)=='-f') then
					read(args(p+1),*) numberOfProc
				elseif(trim(args(p))=='-format') then 
					fmt = trim(args(p+1))		
				elseif(args(p)=='-help') then
					write(*,*) 'Command line options:'
					write(*,*) '-format: format of the vtu file (ascii or binary)'
					write(*,*) '-np: number of processors used '
					write(*,*) 'example: flubio_meshview'
					stop
				endif
	
			enddo
		else
			! Ask for processor number type:
			write(*,*) "number of processes:"
			read (*,*) numberOfProc
		endif

		is=1
		write(proc,'(i0)') numberOfProc
		write(*,'(a)') 'FLUBIO: renumber mesh for '//trim(proc)//' processors...'

		do i=1,numberOfProc

		   if(fmt=='ascii') then

				k=0
				write(procID,'(i0)') (i-1)
				open(1, file='processor'//trim(procID)//'/cellProcAddressing')
					read(1,*) numberOfElements
				close(1)

				allocate(dummyVec(numberOfElements))

				! Renumber mesh
				ie=is+numberOfElements-1

				do j=is, ie
					k=k+1
					dummyVec(k)=j
				enddo

				is=ie+1

				! Write cellProcAddr
				open(1, file='processor'//trim(procID)//'/cellProcAddressing')
					write(1,'(i0)') numberOfElements
					do iElement=1,numberOfElements
						write(1,'(i0)') dummyVec(iElement)
					enddo
				close(1)

				deallocate(dummyVec)

			else 

				k=0
				write(procID,'(i0)') (i-1)
				open(1, file='processor'//trim(procID)//'/cellProcAddressing', form='unformatted')
					read(1) numberOfElements
				close(1)

				allocate(dummyVec(numberOfElements))

				! Renumber mesh
				ie=is+numberOfElements-1

				do j=is, ie
					k=k+1
					dummyVec(k)=j
				enddo

				is=ie+1

				! Write cellProcAddr
				open(1, file='processor'//trim(procID)//'/cellProcAddressing', form='unformatted')
					write(1) numberOfElements
					do iElement=1,numberOfElements				
						write(1) dummyVec(iElement)
					enddo
				close(1)

				deallocate(dummyVec)

			endif 

		enddo

	end program renumberMesh

!********************************************************************************************************************************


