
cmake_minimum_required( VERSION 3.15 )

PROJECT(renumber)

include( $ENV{home_dir_flubio}/cmake/configure/3-compilers.cmake )

# renumber
set(TARGET renumber)

set( src
  renumber.f03
)

set_source_files_properties( ${src} PROPERTIES LANGUAGE Fortran )

# Add executable
add_executable( ${TARGET} ${src} )

set_target_properties( ${TARGET} PROPERTIES COMPILE_FLAGS ${CMAKE_Fortran_FLAGS})


# writeProcAddressingSerialMesh.f03
set(TARGET writeProcAddressingSerialMesh)

set( src
  writeProcAddressingSerialMesh.f03
)

set_source_files_properties( ${src} PROPERTIES LANGUAGE Fortran )

# Add executable
add_executable( ${TARGET} ${src} )

set_target_properties( ${TARGET} PROPERTIES COMPILE_FLAGS ${CMAKE_Fortran_FLAGS})
