	program renumberMesh

		implicit none

		character(10) procID, proc
		character*10, dimension(:), allocatable :: args
	!------------------------------------------------------------------------------------------------------------------

		integer :: i, j, k, p, iElement, numberOfProc, numberOfElements

		integer :: is, ie

		integer, dimension(:), allocatable:: dummyVec

		integer :: fnumber, np, nargs, stat
	!------------------------------------------------------------------------------------------------------------------

		!------------------------!
		! command line arugments !
		!------------------------!

		nargs = command_argument_count()

		if(nargs>0) then

			allocate(args(nargs))

			! get the arguments
			do p=1,nargs
				call get_command_argument(number=p, value=args(p), status=stat)
			enddo

			! look for the number of Processors
			if(args(1)=='-np') then
				read(args(2),*) numberOfProc
			else
				write(*,*) 'FLUBIO: missing argument "-np". Specify the number of processor'
				stop
			endif
		else
			! Ask for processor number type:
			print*,"number of processes:"
			read (*,*) numberOfProc
		endif

		is=1
		write(proc,'(i0)') numberOfProc
		write(*,'(a)') 'FLUBIO: renumber mesh for '//trim(proc)//' processors...'

		do i=1,numberOfProc

		   k=0

		write(procID,'(i0)') (i-1)
		open(1, file='grid/processor'//trim(procID)//'/cellProcAddressing')
			 read(1,*) numberOfElements
		close(1)

		allocate(dummyVec(numberOfElements))

		! Renumber mesh
		ie=is+numberOfElements-1

		do j=is, ie
		   k=k+1
		   dummyVec(k)=j
		enddo

		is=ie+1

		! Write cellProcAddr
		open(1, file='grid/processor'//trim(procID)//'/cellProcAddressing_r')
			 write(1,'(i0)') numberOfElements
			 do iElement=1,numberOfElements
				write(1,'(i0)') dummyVec(iElement)
			 enddo
		close(1)

		deallocate(dummyVec)

		enddo

	end program renumberMesh

!********************************************************************************************************************************


