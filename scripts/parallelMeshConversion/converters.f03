module converters

    use mpi
    use strings
    use m_strings
    use fileHandling

    implicit none

contains

    subroutine convertPoints(filePath, fileName, fileFormat)

        character(len=*) :: filePath, fileName, fileFormat
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, n
    !------------------------------------------------------------------------------------------------------------------    

        real, dimension(:,:), allocatable :: points
    !------------------------------------------------------------------------------------------------------------------

        call readListOfRealFromFile(fileName=filePath//fileName, returnList=points)

        if(fileFormat=='ascii') then

            open(1, file=filePath//fileName)
                write(1,'(i0)') size(points(:,1))
                do n=1,size(points(:,1))
                    write(1,'(9999(g0))') (points(n,i), " ", i=1,3) 
                enddo    
            close(1)

        else
            open(1, file=filePath//fileName, form='unformatted')
                write(1)  size(points(:,1))
                do n=1,size(points(:,1))
                    write(1) points(n,1:3)
                enddo   
            close(1)
        endif        

    end subroutine convertPoints

! *********************************************************************************************************************

    subroutine convertFaces(filePath, fileName, fileFormat)


        type(integerCellStruct), dimension(:), allocatable :: faces
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: filePath, fileName, fileFormat
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, n

        integer :: csize

        integer :: arrayOfInts(25)
    !------------------------------------------------------------------------------------------------------------------    

        call readListOfCellStructFromFile(fileName=filePath//fileName, returnList=faces)

        if(fileFormat=='ascii') then
            
            open(1, file=filePath//fileName)
                write(1,'(i0)') size(faces)
                do n=1,size(faces)
                    csize = faces(n)%csize
                    arrayOfInts(1) = csize
                    arrayOfInts(2:2+csize-1) = faces(n)%col(1:csize)
                    write(1,'(9999(g0))') (arrayOfInts(i), " ", i=1,2+csize-1)  !csize, faces(n)%col(1:csize)
                enddo    
            close(1)

        else
            open(1, file=filePath//fileName, form='unformatted')
                write(1)  size(faces)

                do n=1,size(faces)
                    csize = faces(n)%csize
                    write(1) csize, faces(n)%col(1:csize)
                enddo   
            close(1)
        endif    

    end subroutine convertFaces

! *********************************************************************************************************************

    subroutine convertOwner(filePath, fileName, fileFormat)

        character(len=*) :: filePath, fileName, fileFormat
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
    !------------------------------------------------------------------------------------------------------------------    

        integer, dimension(:), allocatable :: owner
    !------------------------------------------------------------------------------------------------------------------

        call readListOfIntFromFile(fileName=filePath//fileName, returnList=owner)

        if(fileFormat=='ascii') then

            open(1, file=filePath//fileName)
                write(1,'(i0)') size(owner)
                do n=1,size(owner)
                    write(1,'(i0)') owner(n)
                enddo    
            close(1)

        else
            open(1, file=filePath//fileName, form='unformatted')
                write(1)  size(owner)
                do n=1,size(owner)
                    write(1) owner(n)
                enddo   
            close(1)
        endif    

    end subroutine convertOwner

! *********************************************************************************************************************

    subroutine convertNeighbour(filePath, fileName, fileFormat)

        character(len=*) :: filePath, fileName, fileFormat
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
    !------------------------------------------------------------------------------------------------------------------    

        integer, dimension(:), allocatable :: neighbour
    !------------------------------------------------------------------------------------------------------------------

        call readListOfIntFromFile(fileName=filePath//fileName, returnList=neighbour)

        if(fileFormat=='ascii') then

            open(1, file=filePath//fileName)
                write(1,'(i0)') size(neighbour)
                do n=1,size(neighbour)
                    write(1,'(i0)') neighbour(n)
                enddo    
            close(1)

        else
            open(1, file=filePath//fileName, form='unformatted')
                write(1)  size(neighbour)
                do n=1,size(neighbour)
                    write(1) neighbour(n)
                enddo   
            close(1)
        endif    

    end subroutine convertNeighbour

! *********************************************************************************************************************

    subroutine convertBoundaries(id, nid)

        character(10) :: procID
        !! processor ID

        character(len=100) :: val, val2

        integer:: ok, ok1, ok2, ok3, ok4, ok5
    !------------------------------------------------------------------------------------------------------------------

        integer :: id, nid, row, ierr

        integer :: ios, i, istat
        !! error flag to mark the EOF
    !------------------------------------------------------------------------------------------------------------------

    end subroutine convertBoundaries

! *********************************************************************************************************************

    subroutine convertCellAddressing(filePath, fileName, fileFormat)

        character(len=*) :: filePath, fileName, fileFormat
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
    !------------------------------------------------------------------------------------------------------------------    

        integer, dimension(:), allocatable :: cellProcAddr
    !------------------------------------------------------------------------------------------------------------------

        call readListOfIntFromFile(fileName=filePath//fileName, returnList=cellProcAddr)

        if(fileFormat=='ascii') then

            open(1, file=filePath//fileName)
                write(1,'(i0)') size(cellProcAddr)
                do n=1,size(cellProcAddr)
                    write(1,'(i0)') cellProcAddr(n)
                enddo    
            close(1)

        else
            open(1, file=filePath//fileName, form='unformatted')
                write(1)  size(cellProcAddr)
                do n=1,size(cellProcAddr)
                    write(1) cellProcAddr(n)
                enddo   
            close(1)
        endif    

    end subroutine convertCellAddressing

! *********************************************************************************************************************

    subroutine convertFaceAddressing(filePath, fileName, fileFormat)

        character(len=*) :: filePath, fileName, fileFormat
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
    !------------------------------------------------------------------------------------------------------------------    

        integer, dimension(:), allocatable :: faceProcAddr
    !------------------------------------------------------------------------------------------------------------------

        call readListOfIntFromFile(fileName=filePath//fileName, returnList=faceProcAddr)

        if(fileFormat=='ascii') then

            open(1, file=filePath//fileName)
                write(1,'(i0)') size(faceProcAddr)
                do n=1,size(faceProcAddr)
                    write(1,'(i0)') faceProcAddr(n)
                enddo    
            close(1)

        else
            open(1, file=filePath//fileName, form='unformatted')
                write(1)  size(faceProcAddr)
                do n=1,size(faceProcAddr)
                    write(1) faceProcAddr(n)
                enddo   
            close(1)
        endif    

    end subroutine convertFaceAddressing

! *********************************************************************************************************************

    subroutine convertBoundaryProcAddressing(filePath, fileName, fileFormat)

        character(len=*) :: filePath, fileName, fileFormat
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
    !------------------------------------------------------------------------------------------------------------------    

        integer, dimension(:), allocatable :: boundaryProcAddr
    !------------------------------------------------------------------------------------------------------------------

        call readListOfIntFromFile(fileName=filePath//fileName, returnList=boundaryProcAddr)

        if(fileFormat=='ascii') then

            open(1, file=filePath//fileName)
                write(1,'(i0)') size(boundaryProcAddr)
                do n=1,size(boundaryProcAddr)
                    write(1,'(i0)') boundaryProcAddr(n)
                enddo    
            close(1)

        else
            open(1, file=filePath//fileName, form='unformatted')
                write(1)  size(boundaryProcAddr)
                do n=1,size(boundaryProcAddr)
                    write(1) boundaryProcAddr(n)
                enddo   
            close(1)
        endif    


    end subroutine convertBoundaryProcAddressing

! *********************************************************************************************************************

end module converters